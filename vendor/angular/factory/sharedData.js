define("sharedData", ["jquery"], function($) {
  	console.log("Function : sharedData");
	
	var firebaseRoot = "sizzling-fire-8858";
	var firebaseIP  = "https://" + firebaseRoot + ".firebaseio.com";	
	
	var sharedDataObj = {}; 
        sharedDataObj["logState"] = localStorage.getItem("logState");
        phpJS.getPHPStatus(function(state, data){
        	sharedDataObj["phpStatus"] = JSON.parse(data); 
        });
        
 
		  return {
		  	
		  	///////////////////////////////////////
		  	add:function(key, value) {
                    sharedDataObj[key] = value;
			},
			///////////////////////////////////////	
			
		  	///////////////////////////////////////
		  	fetch:function(key) {
                  if ( sharedDataObj[key] == undefined ||  sharedDataObj[key] == null){
                  	return false;
                  }
                  else{
                  	return sharedDataObj[key];
                  }
			},
			///////////////////////////////////////				
			
			
		  	///////////////////////////////////////
		  	remove:function(key) {

			},
			///////////////////////////////////////	
			
		  	///////////////////////////////////////
		  	request:function(component, callback) {
		  		
		  		var counter = 0;
		  		function check(){
		  			setTimeout(function(){
			  			if (sharedDataObj[component] == undefined){
			  				counter++;
			  				if (counter < 20){
			  					check();
			  				}
			  				else{
			  					timeout();
			  				}
			  			}
			  			else{
			  				callback(true, sharedDataObj);
			  			}
		  			}, 50);
		  		}
		  		
		  		function timeout(){
		  			callback(false, "Data has timedout.");
		  		};
				check();
			},
			///////////////////////////////////////	

		  	///////////////////////////////////////
		  	getAll:function() {
				return sharedDataObj;
			}
			///////////////////////////////////////							

    		
    
    
  		};
 
});

