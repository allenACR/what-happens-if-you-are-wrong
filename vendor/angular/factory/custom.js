define("custom", ["jquery", "sharedData", "backstretch"], function($, sharedData) {
  	console.log("Function : custom");
	
	
	
	var firebaseRoot = "sizzling-fire-8858";
	var firebaseIP  = "https://" + firebaseRoot + ".firebaseio.com";
 
 
		  return {
		  	
		  	 ///////////////////////////////////////
		  	 databaseInit:function(callback){
					var checkCount = 0; 
					
					// DATABASE TIMEOUT 
					var checkTimeout = function(){
						callback(false);				
					};								
					
					var checkConnection = function(){
						setTimeout(function(){
						 	var phpStatus = sharedData.getAll().phpStatus;	
						    if (phpStatus == undefined){							    	
						    	checkCount++;
						    	if (checkCount > 20){
						    		checkTimeout();
						    	}
						    	else{
						    		checkConnection();
						    	}							    	
						    }
						    else{
						    	callback(true);										
						    }
					   }, 500);
					};
					checkConnection();	
		  	 },
		  	 ///////////////////////////////////////
		  	
		  	
		  	 ///////////////////////////////////////		  	
		  	 backstretchBG:function(image, d, f){
		  	 	$.backstretch([image], {duration: d, fade: f});
		  	 },
		  	   	 
		  	 backstretchElement:function(target, image, d, f){
		  	 	$(target).backstretch([image], {duration: d, fade: f});
		  	 },
		  	 
		  	 removeBackstretch:function(){		  	 	
		  	 	if($.find('.backstretch')){
  					  $('.backstretch').remove();
  				}
		  	 },
		  	 ///////////////////////////////////////

			///////////////////////////////////////
			parseForDatabase: function(s){
				var check = $.trim(s);
				if (check != undefined && check != null){
					returnString = check.replace(/"/g, "&quot;").replace(/'/g, "&lsquo;");
				} 			    
			    return returnString;
			},		
			parseFromDatabase: function(s){
				var check = $.trim(s);
				if (check != undefined && check != null){
					returnString = check.replace(/&quot;/g, '"').replace(/&lsquo;/g, "'");
				} 	
			    return returnString;
			},						
			///////////////////////////////////////	

		  	 ///////////////////////////////////////
		  	 imageToDataUri:function(img, size, callback) {
						    // create an off-screen canvas
						    var canvas = document.createElement('canvas'),
						        ctx = canvas.getContext('2d');
							var image = new Image();
							image.src = img;
							image.onload = function() {	
								var aspectRatio = this.width / this.height,
									adjustedHeight = size / aspectRatio;
									var dimensions = {
										width: size,
										height: adjustedHeight,
										aspectRatio: aspectRatio
									};	
							    // set its dimension to target size
							    canvas.width = size;
							    canvas.height = adjustedHeight;									
							    ctx.drawImage(image, 0, 0, size, adjustedHeight);
							    callback( canvas.toDataURL(), dimensions ); 
							};
			},
			///////////////////////////////////////	
			
		  	
		  	///////////////////////////////////////
		  	parseFile:function(file, callback){
		  		Papa.parse(file, {
					download: true,
					complete: function(csv) {	
													
		 				var tableInfo = csv.data;
		                var headers = [];
		                var finalObj = {};
		
		                // convert headers into obj key
		                var assemblyList = [];
		                for (i = 0; i < tableInfo[0].length; i++){
		                  value = tableInfo[0][i].toLowerCase();
		                  finalObj[value] = {};
		                  assemblyList.push(value);
		                }
		
		                // grab first left hand column - the keys
		                var keys = [];
		                for (i = 0; i < tableInfo.length; i++){
		                   field = tableInfo[i][0].toLowerCase();
		                   if (field == ''){
		                     keys.push( ("key_" + i) );
		                   }
		                   else{
		                     keys.push(field);
		                   }
		                }
		
		                // assemble into object with key/values
		                for (i = 0; i < tableInfo.length; i++){
		                  for (m = 0; m < tableInfo[i].length; m++){
		                    var value = tableInfo[i][m];
		                    var name = assemblyList[m];
		                    var key = keys[i];
		                    finalObj[name][key] = value;
		                  }
		                }						
						
                    	callback(finalObj);											
					}
				});
		  	},
		  	///////////////////////////////////////
		  	
			///////////////////////////////////////
			buildObjectFromSpreadsheet:function(csv, callback ){

                var tableInfo = csv.data;
                var headers = [];
                var finalObj = {};

                // convert headers into obj key
                var assemblyList = []
                for (i = 0; i < tableInfo[0].length; i++){
                  value = tableInfo[0][i].toLowerCase();
                  finalObj[value] = {};
                  assemblyList.push(value);
                };

                // grab first left hand column - the keys
                var keys = [];
                for (i = 0; i < tableInfo.length; i++){
                   field = tableInfo[i][0].toLowerCase();
                   if (field == ''){
                     keys.push( ("key_" + i) );
                   }
                   else{
                     keys.push(field);
                   }
                }

                // assemble into object with key/values
                for (i = 0; i < tableInfo.length; i++){
                  for (m = 0; m < tableInfo[i].length; m++){
                    var value = tableInfo[i][m];
                    var name = assemblyList[m];
                    var key = keys[i];
                        finalObj[name][key] = value;
                  }
                }

                callback(finalObj);
            },
			///////////////////////////////////////
		
			///////////////////////////////////////
			fetchTimestamp: function(){
					var dateObj = new Date();
					var month = dateObj.getUTCMonth();
					var day = dateObj.getUTCDate();
					var year = dateObj.getUTCFullYear();
					var time = dateObj.getTime(); 
					
					dateObject = {
						month: month,
						day: day,
						year: year,
						time: time
					};
					
					return dateObject;
				
			},
			///////////////////////////////////////
			
			
		  	///////////////////////////////////////
		    offcanvas: function(action) {
		
					var canvas = $('#offCanvasMain'); 
					if (action == "show"){
							canvas.addClass('offcanvas-active');
							canvas.removeClass('offcanvas-hide');
							$('body').css('overflow-y', 'hidden');
							return(true);	
							
					};
					
					if (action == "hide"){
							canvas.removeClass('offcanvas-active');
							canvas.addClass('offcanvas-hide');
							$('body').css('overflow-y', 'auto');
							return(false);
					};			
				
					if (action == "toggle"){
						if (canvas.hasClass('offcanvas-active')){
							canvas.removeClass('offcanvas-active');
							canvas.addClass('offcanvas-hide');
							$('body').css('overflow-y', 'auto'); 
							return(false);	
							
						}else{
							canvas.addClass('offcanvas-active');
							canvas.removeClass('offcanvas-hide');
							$('body').css('overflow-y', 'hidden');
							return(true);
						}
						
					};
				
		    },
		    ///////////////////////////////////////
		
			transitIn:function(){
				
						$('body .row').not('.no-animation').each(function(index){
							$(this)
								.transition({   x: -50, scale: .95, opacity: 0}, 0)
								.transition({   x: 0, scale: 1, opacity: 1, delay: index*150}, 1000);	
						});		
	
			},
		
		  	///////////////////////////////////////
		    transitionStart: function() {

		
		    },
		    ///////////////////////////////////////
		
		  	///////////////////////////////////////
		    transitionEnd: function() {		
				
		    },
		    ///////////////////////////////////////
		
		  	///////////////////////////////////////
		    logger: function(msg) {
				console.log(msg);
		    },
		    ///////////////////////////////////////
		    
		  	///////////////////////////////////////
		    kcode: function() {
				alert("here")
		    },
		    ///////////////////////////////////////    
		  	
		  	///////////////////////////////////////
		    truncateText: function(text, limit ) {
				var myText = text.toString();
				len = myText.length;
				if(len>limit)
				{
					return myText.substr(0,limit)+'...';
				}	
				else{	
					return myText;
				}
		    },
		    ///////////////////////////////////////

			///////////////////////////////////////
			getRandomNumber: function(min, max) {
				return Math.floor( Math.random() * (max - min) + min );
			},			
			///////////////////////////////////////
			
			///////////////////////////////////////
			commaSeparateNumber: function(val){
			    while (/(\d+)(\d{3})/.test(val.toString())){
			      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
			    }
			    return val;
			},			
			///////////////////////////////////////
			
			///////////////////////////////////////
			checkNumberType: function(val){
			    var unit = "";
			    var fixedNum = 0; 

			
				if (val >= 0 && val <100){
			    	unit = "";
			    	fixedNum = val; 	
				};
			    if (val >= 100){
			    	unit = "Hundred";
			    	fixedNum = val; 			    	
			    };
			    if (val >= 1000){
			    	unit = "Thousand";
			    	fixedNum = (val * .001).toFixed(1); 
			    };
			    if (val >= 1000000){
			    	unit = "Million";
			    	fixedNum = (val * .000001).toFixed(1); 
			    };	
			    if (val >= 1000000000){
			    	unit = "Billion";
			    	fixedNum = (val * .000000001).toFixed(1); 
			    };	
			    if (val >= 1000000000000){
			    	unit = "Trillion";
			    	fixedNum = (val * .000000000001).toFixed(1); 
			    };				    
			    
			    return {unit: unit, number: fixedNum};			    		    
			},			
			///////////////////////////////////////			

			
			///////////////////////////////////////
			selfReference: function(scripts){
			    filepath = scripts[ scripts.length-1 ].src; 
				var fileNameIndex = filepath.lastIndexOf("/") + 1;
				var filename = filepath.substr(fileNameIndex);
				filename = filename.replace(/\.[^/.]+$/, "");
				return filename;		
			},
			///////////////////////////////////////
			
			///////////////////////////////////////
    		parallaxStart: function(){
				// Cache the Window object
				$window = $(window);
			                
			   $('div[data-type="background"]').each(function(){
			     var $bgobj = $(this); // assigning the object
			                    
			      $(window).scroll(function() {
			                    
					// Scroll the background at var speed
					// the yPos is a negative value because we're scrolling it UP!								
					var yPos = -($window.scrollTop() / $bgobj.data('speed')); 
					
					var vOffset = $bgobj.data('offset');
					
					
					// Put together our final background position
					var coords = '50% '+ (yPos + vOffset) + 'px';
			
					// Move the background
					$bgobj.css({ backgroundPosition: coords });
					
					}); // window scroll Ends
			
			 });	
			///////////////////////////////////////
			


			
    		}
    
    
  		};
  
 
});


