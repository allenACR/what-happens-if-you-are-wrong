angular.module("angular-gap", []).directive("gap", function() {
    return {
        restrict: "ECA",
        transclude: !0,
        template: '<div class="angular-gap"></div>'
    };
});