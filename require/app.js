define("app", 	[	"custom", "firebaseSettings", "angular", "responsiveTables",
					"uiModalCtrl", "accountModalCtrl",
					
					"ui.router", "ngAnimate", "ngSanitize", "angular-centered", "angular-gap", 
					"sticky", "contenteditable", "mm.foundation", 
					"ngStorage", "toaster", "chieffancypants.loadingBar",	
					"ui.utils", "psResponsive", "angular-carousel", 									
					"ngIdle",
					"stBlurredDialog", "SmoothScroll","adaptive.detection", 
					"firebase", "firebaseSettings",
					
					"FBAngular", 		
					"string", "angularSpinkit", "dcbImgFallback",	
					"gd.ui.jsonexplorer", "urish.load", "adaptive.youtube",
					"angularFileUpload", "truncate",
					"checklist-model", 
					"formly", "ngClickSelect", "multi-select",
					"angular-table", "ngPatternRestrict",  
					"angular-underscore", "textarea-fit", "froala", "lrFileReader", 
					"ng-Linq", "ngBrowserInfo", "ui.tree", "ng-mfb",
					"angularCharts", "ngDonut", "n3-line-chart",
					
					"angucomplete"
					
				], function(custom, firebaseSettings) { 
	custom.logger("Function : app");
	
	
	////////////////
	// INIT 
	var app = angular.module('app', [	"ui.router", "ngAnimate", "ngSanitize", "angular-centered", "angular-gap", 
										"uiModalCtrl", "accountModalCtrl",
										
										"sticky", "contenteditable", "mm.foundation", 
										"ngStorage", "toaster", "chieffancypants.loadingBar", 		  
										"ui.utils", "psResponsive", "angular-carousel", 
										"ngIdle",
										"stBlurredDialog", "SmoothScroll","adaptive.detection", 
										"firebase",
										
										"FBAngular", 		
										"string", "angularSpinkit", "dcbImgFallback",		
										"gd.ui.jsonexplorer", "urish.load", "adaptive.youtube", 
										"angularFileUpload", "truncate", 
										"checklist-model", 
										"formly", "ngClickSelect", "multi-select",
										"angular-table", "ngPatternRestrict", 										
										"angular-underscore", "textarea-fit", "froala", "lrFileReader", 
										"ng-Linq", "ngBrowserInfo", "ui.tree", "ng-mfb",
										"angularCharts", "ngDonut", "n3-line-chart",
										
										"angucomplete"
									]);
										
	app.init = function () {
		
		firebaseSettings.setDefaultUserData(function(){
			angular.bootstrap(document, ['app']);	
		});
		
	           
	};
	////////////////
	
	// let's make a nav called `myOffCanvas`
	app.factory('myOffCanvas', function (cnOffCanvas) {
	  return cnOffCanvas({
	    controller: 'headerCtrl',
	    templateUrl: 'components/offcanvas/offcanvas.php'
	  });
	}).
	
	value('froalaConfig', {
			inlineMode: false,
			events : {
				align : function(e, editor, alignment){
					console.log(alignment + ' aligned');
				}
			}
	}).
	
	// typically you'll inject the offCanvas service into its own
	// controller so that the nav can toggle itself
	controller('MyOffCanvasCtrl', function (myOffCanvas) {
	  this.toggle = myOffCanvas.toggle;
	}).
		
		
	directive('styleParent', function(){ 
	   return {
	     restrict: 'A',
	     link: function(scope, elem, attr) {
	         elem.on('load', function() {
	            var w = $(this).width(),
	                h = $(this).height();
				
					//$('#mainSlider').css('min-height', h + 'px');
					//$('#headerFiller').css('height', h + 'px'  ) ;
	            //check width and height and apply styling to parent here.
	         });
	     }
	   };
	}).	
		
	////////////////  CONFIG
	//
	config(function($stateProvider, $urlRouterProvider) {
			
			// For any unmatched url, redirect to /state1
			$urlRouterProvider.otherwise("/home");
		  
			// Now set up the states
			$stateProvider
			    .state('home', {
			      url: "/home",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },				        
			        "ui-modals": { templateUrl: "layout/modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "layout/modals/account/account-modals.html"
			        			
			        },				        			        			        			      	
			        "header": { templateUrl: "components/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/home/home.php",
			        			controller: "homeController"
			         },			         
			      },	
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },				  			      		      
			      
			    })			    
			    .state('admin', {
			      url: "/admin",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "layout/modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "layout/modals/account/account-modals.html"
			        			
			        },				        					        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/admin/admin.php",
			        			controller: "adminController"
			         },
			      },
			      onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },
			    })
			    
			    .state('page2', {
			      url: "/:type?name",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "layout/modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "layout/modals/account/account-modals.html"
			        			
			        },				        					        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page2/page2.php",
			        			controller: "page2Controller"
			         },
			      },
			      onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },
			    })
			    
			    .state('page3', {
			      url: "/page3",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "layout/modals/ui/ui-modals.html"
			        			
			        },		
			        "account-modals": { templateUrl: "layout/modals/account/account-modals.html"
			        			
			        },				        				        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page3/page3.php",
			        			controller: "page3Controller"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    })
			    
			    .state('page4', {
			      url: "/page4",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "layout/modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "layout/modals/account/account-modals.html"
			        			
			        },				        					        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page4/page4.php",
			        			controller: "page4Controller"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    })
			    
			    .state('page5', {
			      url: "/page5",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "layout/modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "layout/modals/account/account-modals.html"
			        			
			        },				        					        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page5/page5.php",
			        			controller: "page5Controller"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    })

			    .state('page6', {
			      url: "/page6",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "layout/modals/ui/ui-modals.html"
			        			
			        },
			        "account-modals": { templateUrl: "layout/modals/account/account-modals.html"
			        			
			        },				        						        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page6/page6.php",
			        			controller: "page6Controller"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    });		 
			    			    
			    

	    			       
		});
		//
		////////////////   
	
	return app;
	
});

