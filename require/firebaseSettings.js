define("firebaseSettings", ["jquery"], function($) {
	
	var firebaseRoot = "sizzling-fire-8858";
	var firebaseIP  = "https://" + firebaseRoot + ".firebaseio.com";
 
 
		  return {  			
			///////////////////////////////////////
			setDefaultUserData: function(callback){

			 	//SET DEFAULT USER DATA - INIT IN HEADER
			 	var logState = localStorage.getItem("logState");
			 	if (logState == null || logState == "false"){								
					localStorage.setItem("logState", false);
					callback();
		
			 	}
			 	else{
		 			callback();	
			 	};
			},
			///////////////////////////////////////		
		
			///////////////////////////////////////
			startLogin: function(){
				var start = function(){
						stBlurredDialog.open();
						var modalInstance = $modal.open({
					      	templateUrl: 'loginModal.html',
					  		controller: accountModalCtrl.loginModalCtrl(),
							
						});
					
						modalInstance.result.then(function(returnData)
						{	// LOGIN SUCCESSFUL 					
							$scope.logState = true;				 
							stBlurredDialog.close();
					  		custom.logger('Modal dismissed at: ' + new Date());						 		
						}, 
						function (){
							// MODAL DISMISS
							stBlurredDialog.close();
					  		custom.logger('Modal dismissed at: ' + new Date());
						});	
				};
				return start;
			},
			///////////////////////////////////////		
		
			///////////////////////////////////////
			setLoginData: function(data){
					// PULL INFO 
					new Firebase(firebaseIP + '/users/' + data.id + "/info/").once('value', function(snap) {					   
						var data  = snap.val();
						
						// UPDATE SECURITY HASH
						var infoEntry = new Firebase(firebaseIP + '/users/' + data.id + "/info/");
						var securityHash = "";
						for (i = 0; i < 10; i++){	securityHash += Math.random().toString(36).substring(7);};
						infoEntry.update({auth: securityHash});						
						
						// SET LOCAL DATA
						defaultData = {
							id: data.id,
							auth: securityHash
						};
						console.log(defaultData);
						localStorage.setItem("logState", true);
						localStorage.setItem("user", JSON.stringify(defaultData) );				

						location.reload();												
					});	
			},
			///////////////////////////////////////	
			
			///////////////////////////////////////
			setLogout: function(){			
				localStorage.clear();
				location.reload();		
			},
			///////////////////////////////////////		
			
			
			///////////////////////////////////////
			getPermissionLevel: function(callback){
				
				this.checkUserData(function(returnState, data){	
					
					permissionLevel = {
						type: "guest",
						value: 0
					};
					if (returnState){
						var type = data.user.permission.toLowerCase();
						// ACCESS DEFINITIONS				  
						if (type == "user"){
							permissionLevel = {
								type: type,
								value: 1
							};
						}
						if (type == "admin"){
							permissionLevel = {
								type: type,
								value: 5
							};
						}	
						if (type == "superadmin"){
							permissionLevel = {
								type: type,
								value: 10
							};
						}										
					}
					callback( returnState, permissionLevel );

					
				});
				
			},
			///////////////////////////////////////
			
			
			///////////////////////////////////////
			checkUserData: function(callback){
				
				var logState	= localStorage.getItem("logState");
				var passCheck = true; 
				var returnCheck = [false, false];
				var returnData = [
					{user: ""},
					{image: ""}				
				];
				
				// NOT LOGGED IN							
				if (logState == "false"){
					returnFailData(false, "Not logged in."); 
				}
				
				// CHECK USER ID AND AUTH
				var user = JSON.parse(localStorage.getItem("user"));
				if (user != null){
					if(user.id == null || user.id == undefined || user.auth == null || user.auth == null){
							passCheck = false; 
					}
				}
				else{
					passCheck = false; 
				}
				

				// IF FAILED

				
				// RETRIEVE DATA AND CHECK AUTH
				if (passCheck){

					// PULL INFO 
					new Firebase(firebaseIP + '/users/' + user.id + "/info/").once('value', function(snap) {					   
						var data  = snap.val();
							if (data != null){
								// IF AUTHENTICATION MATCHES, PULL DATA
								if (data.auth == user.auth){	
									
																
										userData = {
											id: data.id,
											email: data.email,
											permission: data.permission,
											firstName: data.firstName,
											lastName: data.lastName,
											userName: data.userName
										};
										returnData.user = userData;																
										returnSuccessData(0);	
								}
								
								// FAILED
								else{
									returnFailData(true, "Authentication has failed.");
								}
							}
							else{
								returnFailData(true, "Authentication failure.  Just like you.");
							}							
					});
					
					// PULL IMAGE DATA
					new Firebase(firebaseIP + '/users/' + user.id + "/images/profile/").once('value', function(snap) {					   
						var data  = snap.val();
							if (data != null){
								imageData = {
									small: data.small,
									standard: data.standard,
									thumbnail: data.thumbnail,
								};
								returnData.image = imageData;																
								returnSuccessData(1);	
							}
							else{
								returnFailData(true, "Authentication failure.  Just like you.");
							}
					});					
				}; 		 

				// RETURN SUCCESS
				function returnSuccessData(arrayCheck){
					returnCheck[arrayCheck] = true;
					var pass = true;
					
					// CHECK FOR QUERIES
					for (i = 0; i < returnCheck.length; i++){
						if (returnCheck[i] == false){
							pass = false;
						}
					}
					//  WAITS FOR BOTH CALLS TO BE MADE BEFORE RETURNING
					if (pass){
						callback(true, returnData);	
					};
				};

				// DEFAULT RETURN FAIL DEFAULT
				function returnFailData(isError, errorType){
															
					callback(false, [isError, errorType]);
				}

			},
			///////////////////////////////////////	

		 	
		
			///////////////////////////////////////
			setLoginData: function(data){
					// PULL INFO 
					new Firebase(firebaseIP + '/users/' + data.id + "/info/").once('value', function(snap) {					   
						var data  = snap.val();
						
						// UPDATE SECURITY HASH
						var infoEntry = new Firebase(firebaseIP + '/users/' + data.id + "/info/");
						var securityHash = "";
						for (i = 0; i < 10; i++){	securityHash += Math.random().toString(36).substring(7);};
						infoEntry.update({auth: securityHash});						
						
						// SET LOCAL DATA
						defaultData = {
							id: data.id,
							auth: securityHash
						};
						console.log(defaultData);
						localStorage.setItem("logState", true);
						localStorage.setItem("user", JSON.stringify(defaultData) );				

						location.reload();												
					});	
			},
			///////////////////////////////////////	
			
			///////////////////////////////////////
			setLogout: function(){			
				localStorage.clear();
				location.reload();		
			},
			///////////////////////////////////////	
		  	
		  	///////////////////////////////////////
			fetchLogState: function(callback){
				logState	= localStorage.getItem("logState");
				callback(logState);
			},
			///////////////////////////////////////
		  	
			///////////////////////////////////////
			firebaseBase: function(){
				return firebaseRoot;		
			},
			///////////////////////////////////////
			
		  	///////////////////////////////////////
		    firebaseRoot: function() {
				return firebaseIP;
		    },
		    ///////////////////////////////////////	
		    	 
    
  		};
 
});