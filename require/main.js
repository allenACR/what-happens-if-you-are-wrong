// ESTABLISH REQUIREJS PATHING
require.config({
	
	
	// URL
	baseUrl: '',
	
	// LOCATION OF SCRIPTS
	paths: {
		
		// FIREBASE //		
		"firebase": "https://cdn.firebase.com/libs/angularfire/0.8.2/angularfire.min",			//https://www.firebase.com/quickstart/angularjs.html
		"firebaseAuthenticate": "vendor/firebase/simpleLogin",									//https://www.firebase.com/docs/security/simple-login-email-password.html
		"firebaseJS": "vendor/firebase/firebase",		
		"firebaseSettings": "require/firebaseSettings",	
		
		// CORE FILES
		"angular": "vendor/angular/js/angular-1.2.9.min",				
		"jquery": "vendor/jquery/jquery-2.1.1.min",
		"backstretch": "vendor/backstretch/backstretch",
		"foundation": "vendor/foundation5/js/foundation.min",
		"transitJS": "vendor/angular/module/transit"	,										//http://ricostacruz.com/jquery.transit/		
		"responsiveTables": "vendor/foundation5/js/responsive-tables",	
		"mm.foundation": "vendor/foundation5/js/foundation5",									//http://madmimi.github.io/angular-foundation/													//https://github.com/gsklee/ngStorage
		"app": "require/app",	
					
		// ESSENTIAL BASE MODULES //
		"ngStorage": "vendor/angular/module/ngStorage.min",	
		"ngAnimate": "vendor/angular/module/angular-animation-1.2.2",							//http://code.angularjs.org/1.1.4/docs/api/ng.directive:ngAnimate
		"ngSanitize": "vendor/angular/module/angular-sanitize",									//https://github.com/angular/bower-angular-sanitize	
		"ui.router": "vendor/angular/module/angular-ui-router",									//https://github.com/angular-ui/ui-router	
		"papaParse": "vendor/papaparse/papaparse.min",											//http://papaparse.com/	
		"underscore": "vendor/underscore/underscore",			
		"angular-underscore": "vendor/underscore/angular-underscore.min",						//http://underscorejs.org/	
		"string": "vendor/angular/module/ng-string.min",										//https://github.com/goodeggs/ng-string"
		"truncate": "vendor/angular/module/truncate",											//https://github.com/sparkalow/angular-truncate
		"angular-centered": "vendor/angular/module/angular-centered",							//http://ngmodules.org/modules/angular-centered
		"angular-gap": "vendor/angular/module/angular-gap",
		"stBlurredDialog": "vendor/screenblur/js/st-blurred-dialog",
		"chieffancypants.loadingBar": "vendor/angular/module/loading-bar",						//http://chieffancypants.github.io/angular-loading-bar/
		"adaptive.detection": "vendor/angular/module/angular-adaptive-detection.min",			//https://github.com/angular-adaptive/adaptive-detection
		"ngTouch": "https://ajax.googleapis.com/ajax/libs/angularjs/1.2.9/angular-touch.min",	//https://github.com/angular/bower-angular-touch
		"sticky": "vendor/angular/module/sticky",												//https://github.com/d-oliveros/angular-sticky			
		"SmoothScroll": "vendor/angular/module/ng-smoothscroll.min",
		"angular-carousel": "vendor/angular/module/angular-carousel.min",						//https://github.com/revolunet/angular-carousel
		"textarea-fit": "vendor/angular/module/textareaFit",									//https://github.com/nikolassv/angular-textarea-fit
		"lrFileReader": "vendor/angular/module/fileReader",
		"ng-Linq": "vendor/angular/module/ngLinq",												//https://github.com/ViniciusMachado/ngLinq
		"ngBrowserInfo": "vendor/angular/module/ngBrowserInfo.min",								//https://github.com/transferwise/ng-browser-info
		"angucomplete": "vendor/angular/module/angucomplete",									//https://github.com/darylrowland/angucomplete
		
		// MOMENTJS
		"momentCore":"vendor/moment/moment.min",
		"angularMoment":"vendor/angular/module/angular-moment",
		
		// FROALA TEXT EDITOR
		"froalaJS":"vendor/froala/js/froala_editor.min",
		"froala":"vendor/froala/js/angular-froala",
		"froalaSanitize":"vendor/froala/js/angular-froala-sanitize",
	
		// D3 GRAPHS 
		"d3Core":"vendor/d3/js/d3.min",
		"angularCharts":"vendor/d3/js/angular-charts",											//http://chinmaymk.github.io/angular-charts/
		"ngDonut":"vendor/d3/js/donut",															//https://github.com/Wildhoney/ngDonut
		"n3-line-chart":"vendor/d3/js/lineCharts.min",											//https://github.com/n3-charts/line-chart

		// REMOVE IF NOT NEEDED		
		"urish.load": "vendor/angular/module/angular-load.min",									//https://github.com/urish/angular-load	
		"contenteditable": "vendor/angular/module/angular-contenteditable",						//https://github.com/akatov/angular-contenteditable	
		"ngPatternRestrict": "vendor/angular/module/ng-pattern-restrict",						//https://github.com/AlphaGit/ng-pattern-restrict
		"ui.utils": "vendor/angular/module/ui-utils.min",										//http://angular-ui.github.io/
		"psResponsive": "vendor/angular/module/ps-responsive",									//https://github.com/lavinjj/angular-responsive
		"FBAngular": "vendor/angular/module/angular-fullscreen",								//https://github.com/fabiobiondi/angular-fullscreen		
		"dcbImgFallback": "vendor/angular/module/angular.dcb-img-fallback.min",					//http://ngmodules.org/modules/angular-img-fallback				
		"toaster": "vendor/angular/module/toaster",												//https://github.com/jirikavi/AngularJS-Toaster		
		"angularSpinkit": "vendor/angular/module/angular-spinkit",								//https://github.com/urigo/angular-spinkit
		"gd.ui.jsonexplorer": "vendor/jsonexplorer/js/gd-ui-jsonexplorer",						//https://github.com/Goldark/ng-json-explorer
		"adaptive.youtube": "vendor/angular/module/angular-adaptive-youtube.min",				//https://github.com/angular-adaptive/adaptive-youtube
		
		"angularFileUpload": "vendor/angular/module/angular-file-upload",						//https://github.com/danialfarid/angular-file-upload		
		"checklist-model": "vendor/angular/module/checklist-model",								//https://github.com/vitalets/checklist-model
		"formly": "vendor/angular/module/formly",												//https://github.com/nimbly/angular-formly
		"ngClickSelect": "vendor/angular/module/ng-click-select",								//https://github.com/adjohnson916/ng-click-select
		"multi-select": "vendor/multiSelect/js/angular-multi-select",							//http://isteven.github.io/angular-multi-select/		
		"angular-table": "vendor/angular/module/smartTable.min",								//http://lorenzofox3.github.io/smart-table-website/	
		"ngIdle": "vendor/angular/module/angular-idle",											//http://ngmodules.org/modules/ng-idle
		"angulartexteditor": "vendor/angular/module/angularTextEditor",                         //https://github.com/vitconte/angularTextEditor
		"ui.tree": "vendor/uiTree/js/angular-ui-tree.min",										//https://github.com/JimLiu/angular-ui-tree
		"ng-mfb": "vendor/floatingButtons/mfb-directive",										//https://github.com/nobitagit/ng-material-floating-button/blob/master/src/index.html
			
		// MODALS //
		"uiModalCtrl": "layout/modals/ui/ui-modalControls",									
		"accountModalCtrl": "layout/modals/account/account-modalControls",
		
		// FACTORIES //
		"custom": "vendor/angular/factory/custom",
		"sharedData": "vendor/angular/factory/sharedData",
		"konami": "vendor/angular/factory/konami",
		
		// LAYOUT //
		"overlayCtrl": "layout/overlay/overlay",
		"offcanvasCtrl": "layout/offcanvas/offcanvas",
		"sliderCtrl": "layout/slider/slider",
		
		// PAGE CONTROLLERS //
		// PAGE CONTROLLERS //
		"headerCtrl": "components/header/header",
		"footerCtrl": "components/footer/footer",		
		"homeCtrl": "components/home/home",
		"adminCtrl": "components/admin/adminCtrl",
		
		"page2Ctrl": "components/page2/page2Ctrl",
		"page3Ctrl": "components/page3/page3Ctrl",
		"page4Ctrl": "components/page4/page4Ctrl",
		"page5Ctrl": "components/page5/page5Ctrl",
		"page6Ctrl": "components/page6/page6Ctrl",
	},

    //DEPENDENCIES
    shim: {        
        
        //MODULES//
        "backstretch":{
        	deps: ['jquery']
        },
        "transitJS":{
        	deps: ['jquery']
        },   
		"sharedData":{
        	deps: ['angular', 'firebase', 'firebaseAuthenticate']
        },   
		"custom":{
        	deps: ['angular', 'transitJS', 'momentCore', 'backstretch', 'sharedData']
        },                    
        "responsiveTables": {
        	deps: ['jquery']
        },
        "angular": {
        	deps: ['jquery'],
            exports: "angular"
        },
        "papaParse": {
        	exports: "papaParse"	
        },
        
        "firebaseSettings":{
        	deps: ['angular', 'firebase', 'firebaseAuthenticate']
        },
        "ui.router": {
        	deps: ['angular'],
            exports: "angular"
        },
        "ngAnimate": {
        	deps: ['angular'],
        	exports: "angular"
        },         
        "ngSanitize": {
        	deps: ['angular'],
            exports: "angular"
        },     
        "angular-centered": {
        	deps: ['angular'],
        	exports: "angular"
        },  
        "angular-gap":{
        	deps: ['angular'],
        	exports: "angular"      	
        },
        "sticky": {
        	deps: ['angular'],
        	exports: "angular"
        },                          
        "contenteditable": {
        	deps: ['angular'],
        	exports: "angular"
        },  
        "mm.foundation": {
        	deps: ['angular'],
        	exports: "angular"
        },
        "ui.utils": {
        	deps: ['angular'],
            exports: "angular"
        },          
        "psResponsive": {
        	deps: ['angular'],
        	exports: "angular"
        },  
        "angular-carousel": {
        	deps: ['angular', 'ngTouch'],
        	exports: "angular"	
        }, 
        "FBAngular": {
        	deps: ['angular', 'ngTouch'],
        	exports: "angular"	
        },    
        "chieffancypants.loadingBar": {
        	deps: ['angular', 'ngTouch'],
        	exports: "angular"	
        },  
        "dcbImgFallback":{
        	deps: ['angular'],
        	exports: "angular"
        },
        "adaptive.detection": {
        	deps: ['angular'],
        	exports: "angular"
        }, 
        "ngStorage": {
        	deps: ['angular'],
        	exports: "angular"	
        },  
        "toaster": {
        	deps: ['angular'],
        	exports: "angular"	
        },                            
        "ngTouch": {
        	deps: ['angular'],
        	exports: "angular"
        },    
        "string":{
        	deps: ['angular'],
        	exports: "angular"	
        },
        "angularSpinkit":{
			deps: ['angular'],
        	exports: "angular"        	
        },
        "gd.ui.jsonexplorer":{
        	deps: ['angular'],
        	exports: "angular"
        },
        "urish.load":{
        	deps: ['angular'],
        	exports: "angular"	
        },
        
        "angularFileUpload":{
        	deps: ['angular'],
        	exports: "angular"
        },        
        "truncate":{
        	deps: ['angular'],
        	exports: "angular"
        },    
        
        "checklist-model":{
			deps: ['angular'],
        	exports: "angular"     	
        },
        "formly":{
			deps: ['angular'],
        	exports: "angular"     	
        },   
        "ngClickSelect":{
			deps: ['angular'],
        	exports: "angular"   	
        },  
        "multi-select":{
			deps: ['angular'],
        	exports: "angular"   	
        },  
  
        "angular-table":{
			deps: ['angular'],
        	exports: "angular"    	
        },    
        "ngPatternRestrict":{
			deps: ['angular'],
        	exports: "angular"    	
        }, 
        "ngIdle":{
			deps: ['angular'],
        	exports: "angular"    	
        }, 
        "angular-underscore":{
			deps: ['angular', 'underscore'],
        	exports: "angular"         	
        },
        "textarea-fit":{
			deps: ['angular', 'jquery'],
        	exports: "angular"         	
        },
        "froalaJS":{
			deps: ['jquery']      	
        },
        "froalaSanitize":{
			deps: ['angular', 'froalaJS',],
        	exports: "angular"         	
        },
        "froala":{
			deps: ['froalaSanitize'],
        	exports: "angular"         	
        },  
        "lrFileReader":{
			deps: ['angular'],
        	exports: "angular"       	
        }, 
        "ng-Linq":{
			deps: ['angular'],
        	exports: "angular"       	
        },         
         "ngBrowserInfo":{
			deps: ['angular'],
        	exports: "angular"       	
        },   
        "ui.tree":{
			deps: ['angular'],
        	exports: "angular"   	
        },      
        "ng-mfb":{
			deps: ['angular'],
        	exports: "angular"  	
        },   
        "angucomplete":{
			deps: ['angular'],
        	exports: "angular"  	
        }, 
             
               
        
        // D3
        "angularCharts":{
			deps: ['angular', 'd3Core'],
        	exports: "angular"    	
        },
        "ngDonut":{
			deps: ['angular', 'd3Core'],
        	exports: "angular" 
        },
        "n3-line-chart":{
			deps: ['angular', 'd3Core'],
        	exports: "angular" 
        },
    
             
        // UNIQUE
        "uiModalCtrl":{
			deps: ['angular'],
        	exports: "angular"   	
        },
        "accountModalCtrl":{
			deps: ['angular'],
        	exports: "angular"   	
        },
     
		
		//DEPENDENCIES//
      	"stBlurredDialog":{
			deps: ['angular'],
        	exports: "angular"      		
      	},
    	"SmoothScroll":{
			deps: ['angular'],
        	exports: "angular"  	
    	},
    	"adaptive.youtube":{
			deps: ['angular'],
        	exports: "angular"  		
    	},
    	
    	
    	//FIREBASE
        "firebase": {
			deps: ['angular', 'firebaseJS', 'firebaseAuthenticate'],
        	exports: "angular"        	
        }     
    }	
	
});
 	 


// INITALIZE ANGULAR IN CONTROLLERS
require(	[	// DEPENDENCIES
				'app', 'firebase',  
 				
 				// OVERLAY	
				'overlayCtrl', 'offcanvasCtrl', 'sliderCtrl', 
				
				// PAGES
				'headerCtrl', 'footerCtrl','homeCtrl', 'adminCtrl',
				'page2Ctrl',
				'page3Ctrl',
				'page4Ctrl',
				'page5Ctrl',
				'page6Ctrl'
				 
			], 
				
	function (	app, firebase, 
				
				// OVERLAY
				overlayCtrl, offcanvasCtrl, sliderCtrl, 
				
				// HEADER/FOOTER
				headerCtrl, footerCtrl, 
				
				// PAGES
				homeCtrl,
				adminCtrl,
				page2Ctrl,
				page3Ctrl,
				page4Ctrl,
				page5Ctrl,
				page6Ctrl
				
			) {
	
				// OVERLAY CONTROLLERS
				overlayCtrl.apply(app);
				offcanvasCtrl.apply(app);
				sliderCtrl.apply(app);
				
				// HEADER/FOOTER
				headerCtrl.apply(app);
				footerCtrl.apply(app);
						
				// PAGES
				homeCtrl.apply(app);				
				adminCtrl.apply(app);				
				page2Ctrl.apply(app);
				page3Ctrl.apply(app);
				page4Ctrl.apply(app);
				page5Ctrl.apply(app);
				page6Ctrl.apply(app);
				
				app.init();
	
});


