<div class="" style='min-height: 800px' ng-init="init()">

	<div id='content-page' ng-class="{false: 'hideMe'}[fullyLoaded]" style='background-color: rgba(0, 152, 87, .9); padding-top: 50px; padding-bottom: 100px;'>

		<!-- PAGE NAME -->
		<div class="row">
			<div class="small-12 columns">
					<h1>User Account</h1>				
					<br>
					
					
			</div>		
		</div>
		
		<!-- LOADING SCREEN -->
		<div class="row" ng-if="pageStatus.isLoading == true">
							<centered>
									<small>Loading information...</small>
									<br> 		
									<wandering-cubes-spinner></wandering-cubes-spinner>									
							</centered>		
		</div>
		<hr>
		
		
		<!-- NOT AN ADMIN -->
		<div class="row" ng-if="accessLevel.value == 0">		
			<div class="small-12 columns">
					
					<div class="row">		
						<div class="small-12 columns">
								
						    	<h3>Hello, and welcome.  To create a new account, click here.</h3>
						    		<centered>	    
										<button ng-click="createNewAccount()" class="button"><small>Create New Account</small></button>
									</centered>	    
						</div>			
					
					</div>
			    		
					    
			</div>			
		</div>	
		
		<!-- ADMIN -->
		<div class="row" ng-if="accessLevel.value >= 1">		
			<div class="large-8 small-12 columns pull-left">
			    	<h3>Welcome {{userData.firstName}}.</h3>	    
			</div>	
			
			<div ng-if="userEntries != null" class='large-4 small-4 columnst pull-right' style='padding-right: 10px;'>
					<angucomplete 
						id="ex1"				
				        placeholder="Search database"
				        pause="100"
				        selectedobject="searchObj"
				        localdata="userEntries"
				        searchfields="name"
				        descriptionfield="root"
				        imagefield=""
				        titlefield="name"
				        minlength="1"
			            inputclass="form-control form-control-small"
	            	/>            	  
		   </div>  		
		   
			
			<div class='small-12 columns pull-left'>
				<div ng-if="userEntries == null">
					<h4>You have no entries.  Create one?</h4>
				</div>
				<div ng-repeat="entry in userEntries">
					<button ng-click='lookupEntry(entry.name)'>{{entry.name}} | {{entry.status}}</button>
				</div>	          	  
		  	</div> 
		   
		   <hr>
	
			<!-- INFO -->
			<div ng-class='{false: "vanish", true: "reveal"}[checkBox.preview]'>
				<div ng-include src="'fragments/content/info.html'"></div>
				<div class='small-12 pull-left'>
					<button class='button expand' ng-click='editEntry()'>Edit</button>
					<button class='button expand success' ng-click='changeStatus("approved")' ng-if='accessLevel.permission == "superadmin"'>Approve</button>
					<button class='button expand alert' ng-click='changeStatus("rejected")' ng-if='accessLevel.permission == "superadmin"'>Reject</button>
				</div>
			</div>	
		</div>		
					
	</div>	
	
				
	<gap></gap>
	<gap></gap>
	<gap></gap>
	<gap></gap>
	
</div>	

