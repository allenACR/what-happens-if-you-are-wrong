define(['custom', 'sharedData', 'firebaseSettings'], function(custom, sharedData, firebaseSettings) {

	
	var fileName  = 'admin';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, $location, $state, cfpLoadingBar, $firebase, $modal, stBlurredDialog, accountModalCtrl) {	   
		       
					// GLOBALS
					$scope.siteComponents = [false];
					$scope.pageStatus = {
						isLoading: true,
						hasData: '',						
						hasErrors: false,	
						errorMsg: ''										
					};	
				    $scope.checkBox = {
				    	afterlife: true,
				    	core: true,				    	
				    	facts: true,
				    	odds: false,
				    	preview: false
				    };					
					
					$scope.queryResults = {
							root: '',
							name: ''
					};
					$scope.typeAhead = null;	
					$scope.fullyLoaded = false;
					$scope.isSearching = false;
					$scope.greetings = "Thinking!  It hurts!";						
									
					
					// INIT
					$scope.init = function(){
						custom.transitIn();		
						cfpLoadingBar.start();
						custom.backstretchBG("http://lorempixel.com/1600/1200/abstract/", 0, 1000);
						custom.databaseInit(function(status){
							if (status){
								$scope.start();
							}
						});		
					};	
					
					$scope.start = function(){
						$scope.checkLoad();
						// GET USER AND ACCESS
						sharedData.request("user", function(state, data){
							if(state){
								$scope.userData = data.user;																	
							}
							else{
								$scope.userData = {
									id: null,
									email: null,
									firstName: null,
									lastname: null,
									permission: 'guest',										
									userName: 'anonymous'
								};
							};

							
							sharedData.request("access", function(state, data){	
								if(state){
									console.log(data.access);
									$scope.accessLevel = data.access;
						  			$scope.pageStatus.isLoading = false;	
									$scope.siteComponents[0] = true;
									$scope.checkLoad();											
									
								}									
							});							
								
							
								
						});
						
					};
					
	
					// CHECK LOADED
					$scope.checkLoad =  function() {
						var isSiteLoaded = true;
						var i = $scope.siteComponents.length; while(i--){
							if ( $scope.siteComponents[i] == false){
								var isSiteLoaded = false; 
							}
						}
						if (isSiteLoaded){
							$scope.loadComplete();
						}						
					};	
					
					// LOAD COMPLETE
					$scope.loadComplete = function(){
							if ($scope.accessLevel.value > 0){
			  					$scope.fetchTypeahead(function(data){	
			  						$scope.userEntries = data;		
			  						$scope.$apply();					
			  					});
			  				}  							
						
							cfpLoadingBar.complete();		
							$scope.animatePage();
							$scope.fullyLoaded = true; 
							$scope.$apply();	
					};						
					
					
					// TRANSIT
					$scope.animatePage = function(){
							$('#content-page')
								.transition({   x: -20,  opacity: 0, delay: 500}, 0)
								.transition({   x: 0,   opacity: 1, height: "auto", delay: 150}, 1000);							
					};					
					
					
					// WATCH FOR CHANGES
					$scope.searchObj = {};
					$scope.$watch('searchObj', function() {
						if ($scope.searchObj.title != null){
							var query = "SELECT * FROM db WHERE name='" + $scope.searchObj.title.toLowerCase() + "'"; 
							$scope.searchEntry(query, function(data){
								$scope.setCurrent(data[0]);	
							});						      
						}
					});						
					

				    
					// FETCH TYPEAHEAD 
					$scope.fetchTypeahead = function(callback){
						
						if ($scope.accessLevel.permission == "user"){
							var query = "SELECT * FROM db WHERE submitterID=" +$scope.userData.id + "";
					   	}
					   	if ($scope.accessLevel.permission == "superadmin"){
					   		var query = "SELECT * FROM db ORDER BY submissionDate";
					   	}			
					   	var packet = {
					   		query: query,
					   		database: "codeandl_religion"
					   	};
							 	
					   	phpJS.queryDatabase(packet, function(state, data){	   		
					   		if (state){
					   			// user has no entries
					   			if (data.status == "error"){						   				
					   				callback(null); 	
					   			}		
					   			else{
					   				callback(data);
					   			}
					   		}							   		
					   	});								
					};	 
					
					
					//////////////////////  CREATE NEW ACCOUNT
					$scope.createNewAccount = function(){
							ctrl = accountModalCtrl.createModalCtrl();
							stBlurredDialog.open();
							
							var modalInstance = $modal.open({
						      	templateUrl: 'createModal.html',
						  		controller: ctrl,
							    resolve: {
							        data: function () {
							        // return $scope.deleteUserObj;
							        }
							    }
							});
						
						
							modalInstance.result.then(
								function (returnData) {  // CLOSE
									toaster.pop("success", "User has been created!", "");
									stBlurredDialog.close();
									custom.logger('Modal dismissed at: ' + new Date());
									}, 
								function () {			// DISMISS
									stBlurredDialog.close();
						  			custom.logger('Modal dismissed at: ' + new Date());
						    });
					
		
					};
					////////////////////// 



				
					
					$scope.lookupEntry = function(name){
	
						var query = "SELECT * FROM db WHERE name='" + name.toLowerCase() + "'"; 
						$scope.searchEntry(query, function(data){
							// no match - fetch random
							if (data.status == "error"){
								alert("Could not retrieve entry.");
							}
							// found, set 
							else{
								$scope.setCurrent(data[0]);									
							}
							
						});							
						
					};
					
					// SEARCH FOR ENTRY
					$scope.searchEntry = function(query, callback){
					   	$scope.isSearching = true;
					   	var packet = {
					   		query: query,
					   		database: "codeandl_religion"
					   	};						   
					   	phpJS.queryDatabase(packet, function(state, data){
					   		$scope.isSearching = false;
					   		if (state){		
					   			callback(data);
					   		}
					   	});											
					};	
					
					$scope.setCurrent = function(entry){
						$scope.queryResults = entry; 

						// GET MOMENT
						$scope.entryDate = moment($scope.queryResults.submissionDate, "YYYYMMDD").fromNow();
							
						
						// CLEAN UP 
						$scope.queryResults.submitterEmail = custom.parseFromDatabase(entry.submitterEmail);
						$scope.queryResults.description = custom.parseFromDatabase(entry.description);
						$scope.queryResults.diety = custom.parseFromDatabase(entry.diety);
						$scope.queryResults.heaven = custom.parseFromDatabase(entry.heaven);
						$scope.queryResults.hell = custom.parseFromDatabase(entry.hell);
						

						if ( $.trim($scope.queryResults.image).length > 0){						
							$scope.logoImage = $location.host() + "../" +	$scope.queryResults.image;
						}
						else{
							$scope.logoImage  = "media/fallback/noPreview.png";
						}
						
						$scope.checkBox.preview = true;
						$scope.$apply();
						
					};					
					
					
					$scope.editEntry = function(){
						var result = { type:'edit', name: $scope.queryResults.name };
						$state.go('page2', result );						
					};
					
					$scope.changeStatus = function(status){
						console.log("Approve Edit");
					   	var packet = {
					   		query: "UPDATE db SET status='" + status + "' WHERE id=" + $scope.queryResults.id,
					   		database: "codeandl_religion"
					   	};						   
					   	
					   	phpJS.queryDatabase2(packet, function(state, data){
					   		$scope.isSearching = false;
					   		console.log(state, data)
					   		if (state){
					   				
					   			//callback(data);
					   		}
					   	});							
						
						
					};
				   

				});
						
	    },
	    ///////////////////////////////////////
  };
});
