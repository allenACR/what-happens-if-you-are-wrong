define(['custom'], function(custom, app) {
	var fileName  = 'footer';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $modal, cfpLoadingBar, stBlurredDialog, SmoothScroll, toaster, uiModalCtrl, browserInfo) {	   
				    
					// variables
					$scope.nearBottom = false;
					$scope.footerState = false;
					$scope.browserDetails = browserInfo.giveMeAllYouGot(); 

					//////////////////////
					$scope.open = function () {			
							stBlurredDialog.open();
							var control = uiModalCtrl.browserInfoCtrl(); 
	
							var modalInstance = $modal.open({
						      	templateUrl: 'browserInfoModal.html',
						  		controller: control,
							    resolve: {
							        data: function () {
							       
							        }
							    }
							});
						
						
							modalInstance.result.then(
								function (returnData) {  // CLOSE																											
									stBlurredDialog.close();
									
									}, 
								function () {			// DISMISS	
									stBlurredDialog.close();
						  			
						    });	
						    

						
					};
					//////////////////////	
					
					//////////////////////						
					$scope.init = function(){
						
						//REMOVE LATER - THIS IS FOR TESTING
						//$scope.browserDetails.mobile = true;
					};
					//////////////////////	
					
					

					// watch if at bottom of screen
					//////////////////////
					function watchChange(state){
						
					 if($scope.nearBottom != state){					 	
					 	$scope.nearBottom = state;
					 	$scope.$apply();
					 }
					};		
									
					
					$(window).scroll(function() {	
					   if($(window).scrollTop() + $(window).height() > $(document).height() - 50) {
					   	watchChange(true);
					   }
					   else{
					   	watchChange(false);
					   }
					   
					});
					//////////////////////
				    
				});
				

				
				
				
							
								
	    },
	    ///////////////////////////////////////
  };
});
