<style>
	.sneakyfooter{
		position: fixed; 
		bottom: 0px; 
		width: 100%;
		background-color: rgba(0, 0, 0, 1);
	}

	.footer-reveal{
		bottom: 0px; 
		transition: .5s;
	}
	
	.footer-vanish{
		bottom: -120px; 
		transition: .5s;
	}	
	
	.footer-vanish-mobile{
		bottom: -150px; 
		transition: .5s;
	}	
	
	
</style>



<div class="" style='' ng-init="init()">
	<gap></gap>

	<div ng-if="browserDetails.mobile == true">
		<div style='position: fixed; bottom: 0px; right: 0px; width: 25%; height: 55px; padding-right: 10px; z-index: 99999'>
			<i class="fa fa-3x pull-right white-font" ng-click='footerState = !footerState' ng-class="{true: 'fa-caret-square-o-down', false: 'fa-caret-square-o-up'}[footerState]"></i>
		</div>
		
		<div class="small-12 sneakyfooter" ng-class='{true: "footer-reveal", false: "footer-vanish-mobile"}[footerState]'  >
		
			<div id="trueBottom" class="flat-secondary fonttype-2 small-8 small-offset-2" >
				<centered>
					<br><br>
					<small>Code & Logic is created by Allen Royston</small><br>
					<small>2012 - 2014</small>
					<br><br>
					<a class="button tiny" ng-click="open()">Browser Info</a>		
				</centered>
			</div>
		
		</div>		
		
		
	</div>
	
	
	<div ng-if="browserDetails.mobile == false">
		<div class="small-12 sneakyfooter" ng-class='{true: "footer-reveal", false: "footer-vanish"}[nearBottom]'  >
		
			<div id="trueBottom" class="flat-secondary fonttype-2" >
				<centered>
					<br><br>
						<center><i class="fa fa-sort"></i></center>
					<br><br>
					<small>Code & Logic is created by Allen Royston | 2010-2014</small><br>
					<small>Thanks for checking out my site!</small>
					<br><br><br>			
					<a class="button tiny" ng-click="open()">Browser Info</a>		
				</centered>
			</div>
		
		</div>
	</div>		


</div>