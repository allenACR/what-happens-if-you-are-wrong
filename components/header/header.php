<div ng-init="init()">
	
	<!--
		<nav mfb-menu position="{{pos}}" effect="{{chosenEffect}}" label="" active-icon="fa fa-caret-square-o-up" resting-icon="fa fa-caret-square-o-down"  ng-repeat="pos in positions">
				 <a mfb-button icon="{{button.icon}}" label="{{button.label}}" ng-repeat="button in buttons" ui-sref="{{button.href}}" ></a>	
		</nav>    
	-->
	

	<div class="" style="height:0px; background-color: rgba(0, 0, 0, .2)"></div>
	
	<div class="" sticky id="headerMain"  style="width: 100%; z-index: 99999; "> 
			
			<div class="header-bar fonttype-1" >
				<div class="row no-animation">
					<div class="small-2 pull-left columns">
							<a class='pull-left columns' ui-sref="home"><i class="fa fa-home"></i> Home</a>
					</div>
					<div class="small-2 pull-left columns">
							<a  class='pull-left'  ui-sref="page2({type: 'create', name: null})"><i class="fa fa-cloud-upload"></i> Submit</a>
					</div>
					<div class="small-2 pull-left columns">
							<a  class='pull-left'  ui-sref="about"><i class="fa fa-flash"></i> About</a>
					</div>			
			
			
					<div class="small-2 columns pull-right">			
						<div ng-class="{false: 'visable', true: 'hidden'}[pageStatus.isLoading]">							
							<a  class='pull-right' ng-click="login()" ng-class="{true: 'hidden', false: 'visible'}[logState]"><i class="fa fa-user"></i> Login</a>					
							<a  class='pull-right' ng-click="logout()" ng-class="{false: 'hidden', true: 'visible'}[logState]"><i class="fa fa-power-off"></i> Logout</a>
						</div>
					</div>		
					
					<div class="small-2 columns pull-right">
							<a class='pull-right' ui-sref="admin" ><i class="fa fa-thumb-tack"></i> My Account</a>		
					</div>
					
					

					
					
				</div>
					
			</div>
			
			
	</div>
	
	
</div>
