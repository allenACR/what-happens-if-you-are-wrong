define(['custom', 'firebaseSettings', 'sharedData'], function(custom, firebaseSettings, sharedData, app) {
	var fileName  = 'header';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
			
				app.controller(fileName + 'Controller', function($scope, $timeout, $rootScope, psResponsive, $localStorage, $sessionStorage, $detection, stBlurredDialog, $modal, toaster, uiModalCtrl, accountModalCtrl) {	   
				   $scope.fileName = fileName;
				  

					///////////////////////	  INIT
					$scope.pageStatus = {
						isLoading: true,
						hasData: '',						
						hasErrors: false,	
						errorMsg: ''										
					};

					$scope.init = function(){
				  		// get permission level from server	
			  			firebaseSettings.getPermissionLevel(function(isUser, details){			  				
			  				$scope.accessLevel = {
			  					isUser: isUser,
			  					permission:  details.type,
			  					value: details.value
			  				};
			  					
			  				$scope.getUserDetails();
			  				
			  				// IF ADMIN
			  				if ($scope.accessLevel.value == 5){
			  					
			  				}  							  				
			  			});					  		
				  					  		  		
			  			// GET LOGSTATE
			  			 firebaseSettings.fetchLogState(function(state){				  			 		
			  			 	$scope.logState = state;				  			 		
			  			 });				  		
			  		
					};
					///////////////////////	 
					
					
					// GRAB USER DETAILS IF LOGGED IN	
					$scope.getUserDetails = function(){
				  		$scope.userData = [];				  		  		
				  		firebaseSettings.checkUserData(function(returnState, data){	

							sharedData.add("access", $scope.accessLevel  );
							sharedData.add("user", data.user);
							
				  			if (returnState){		  							  							  			
				  				$scope.userData = data.user;
				  				$scope.userImage = data.image;				  					
				  			}else{				  								  			
				  				$scope.pageStatus.hasErrors  = data[0];
				  				$scope.pageStatus.errorMsg  = data[1];				  				
				  			} 	
				  			$scope.pageStatus.isLoading = false; 
				  			
				  				
				  											
						});
					};					
					
		 			
		 			////////////////////// LOGOUT
		 			$scope.logout = function(){
		 				stBlurredDialog.open();
		 				
			 				var checkOnce = false; 
			 				var fbLogin = new Firebase( firebaseSettings.firebaseRoot() );
							var auth = new FirebaseSimpleLogin(fbLogin, function() {
									if (!checkOnce){
										checkOnce = true;			
										firebaseSettings.setLogout();								
						 			} 	
								
							});	
			 				auth.logout();
		 				
		 			};
		 			//////////////////////
	  
				 	////////////////////// OPEN LOGIN MODAL			 	
					$scope.login = function () {
						stBlurredDialog.open();
						var modalInstance = $modal.open({
					      	templateUrl: 'loginModal.html',
					  		controller: accountModalCtrl.loginModalCtrl(),
							
						});
					
						modalInstance.result.then(function(returnData)
						{	// LOGIN SUCCESSFUL 					
							$scope.logState = true;				 
					  		custom.logger('Modal dismissed at: ' + new Date());						 		
						}, 
						function (){
							// MODAL DISMISS
					  		custom.logger('Modal dismissed at: ' + new Date());
						});
					};
					//////////////////////		



					//  CORNER NAV
				    $scope.positions = ['br'];
				    $scope.buttons = [{
				      label: 'Home',
				      icon: 'fa fa-home',
				      href: 'home'
				    },{
				      label: 'Submit An Entry',
				      icon: 'fa fa-cloud-upload',
				      href: 'page2'
				    },{
				      label: 'Admin',
				      icon: 'fa fa-rebel',
				      href: 'admin'
				    }];
				    $scope.chosenEffect = 'zoomin';		


				  	// DETECT AND APPLY STICKY IF IT WORKS
				  	if ($detection.isAndroid()){
				  		$scope.stickyWorks = true;
				  	}
				  	else if($detection.isiOS()){				  		
				  		$scope.stickyWorks = false;
				  	}
				  	else if($detection.isWindowsPhone()){
				  		$scope.stickyWorks = true;
				  	}
				  	else{				  		
				  		// PC OR DESKTOP
				  		$scope.stickyWorks = true;
				  	}
				   
				    $rootScope.responsive = psResponsive;
					$scope.offcanvasToggle = function(){
						 stBlurredDialog.open();	
							custom.offcanvas('toggle');
					};
				   
				   

					
				});			
				
				



				
								
					
	    },
	    ///////////////////////////////////////
  };
});
