define(['custom', 'sharedData'], function(custom, sharedData) {

	
	var fileName  = 'home';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, $location, cfpLoadingBar, $location, browserInfo, SmoothScroll, browserInfo, angularLoad) {	   
				    
				    // SETUP
				    if (sharedData.fetch("css") == false){
				    	$('body').hide();
				    }
				    $scope.siteComponents = [false, false];
				    $scope.browserDetails = browserInfo.giveMeAllYouGot(); 
				    $scope.userDetails = browserInfo.giveMeAllYouGot(); 
				    $scope.totalNumbers = {
				    	population: 7200000000,
				    	religions: 1000
				    };
				    $scope.checkBox = {
				    	afterlife: true,
				    	core: false,				    	
				    	facts: true,
				    	odds: false
				    };
				    $scope.selectObj = {
				    	active: ''
				    };
					$scope.queryResults = {
							subname: '',
							name: '',
							description: '',
							image: ''
					};
					$scope.donutData = [  					  	
	    					{ name: 'Total Population:', numbers: $scope.totalNumbers.population },
	    					{ name: '', numbers: 0 },
					];		
					$scope.donutColours = ['purple', 'orange', 'red', 'blue'];								
					$scope.typeAhead = null;	
					$scope.fullyLoaded = false;
					$scope.isSearching = false;
					$scope.greetings = "Thinking!  It hurts!";	
					$scope.logoImage = 'media/fallback/noPreview.png';							
							    
				   	$scope.percentage = (1/$scope.totalNumbers.religions);
				    $scope.showSearch = true; 
				    
					// INIT
					$scope.init = function(){	
						
						SmoothScroll.$goTo(0);
						cfpLoadingBar.start();		
						custom.backstretchBG("media/green-bg.jpg", 0, 1000);
						custom.databaseInit(function(status){
							if (status){	
								$scope.loadCSS(function(state){
									if (state){
										$scope.start();	
										 $('body').show();	
									}
									else{
										alert("Could not load CSS.");
									}
								});						
								
							}
						});	
						
						phpJS.getIPAddress(function(state, data){
							//console.log(JSON.parse(data) );
						});		
						
					
						
					};	
						
					// WEBSITE START
					
					$scope.start = function(){	
						$scope.checkLoad();
			
						$scope.fetchTypeahead(function(data){
							$scope.typeAhead = data;
							$scope.siteComponents[0] = true;
							$scope.checkLoad();
						});	
						var routeName = $location.search().type;
							
						// GET RANDOM IF NO ROUTE 
						if (routeName == undefined || routeName == null || routeName == ''){
								$scope.fetchRandomEntry(function(data){
									cfpLoadingBar.complete();
									$scope.setCurrent(data[0]);	
									$scope.siteComponents[1] = true;
									$scope.checkLoad();														
								});
							}
							
						// SEARCH
						else{		
								var query = "SELECT * FROM db WHERE name='" + routeName.toLowerCase() + "'"; 
								$scope.searchEntry(query, function(data){
									// no match - fetch random
									if (data.status == "error"){
										$scope.fetchRandomEntry(function(data){
											cfpLoadingBar.complete();
											$scope.setCurrent(data[0]);	
											$scope.siteComponents[1] = true;
											$scope.checkLoad();					
										});
									}
									// found, set 
									else{
											cfpLoadingBar.complete();
											$scope.setCurrent(data[0]);	
											$scope.siteComponents[1] = true;
											$scope.checkLoad();										
									}
									
								});														
						}	
						

						
					};
						
					// LOAD SPECIFIC CSS
					$scope.loadCSS = function(callback){
						// CHECK IF IT ALREADY EXISTS
						if($scope.browserDetails.mobile){
					    	var custom_css = 'css/custom-mobile.css';
					   	}
					   	else{
					   		var custom_css = 'css/custom.css';
					   	}
					   	
					    // CHECKS IT ALREADY LOADED 	
					   	if (sharedData.fetch("css") == false){
					   		if (sharedData.fetch("css") != custom_css){
						   		sharedData.add("css", custom_css);
								angularLoad.loadCSS(custom_css).then(function() {
								   callback(true);
								}).catch(function() {
								   callback(false);
								});	
							}
							else{
								callback(true);
							}
						}
						else{
							callback(true);
						}												
					};
		            
		            				    					
					// CHECK LOADED
					$scope.checkLoad =  function() {
						var isSiteLoaded = true;
						var i = $scope.siteComponents.length; while(i--){
							if ( $scope.siteComponents[i] == false){
								var isSiteLoaded = false; 
							}
						}
						if (isSiteLoaded){
							$scope.loadComplete();
						}
						$scope.fullyLoaded = isSiteLoaded;
						
					};						
												
					$scope.loadComplete = function(){							
							
							// REMOVE LATER 
							//$scope.browserDetails.mobile = true;
							
							sharedData.request("user", function(state, data){
								if(state){									
									$scope.userData = data.user;	
									$scope.userData['accessLevel'] = data.access.value;																								
								}
								else{
									$scope.userData = {
										id: null,
										email: null,
										firstName: null,
										lastname: null,
										permission: 'guest',										
										userName: 'anonymous',
										accessLevel: 0
									};
								}
								
								$scope.$apply();
								
							});
							
							
						
							custom.parallaxStart();	   
							$('#search_nav')
								.transition({ opacity: 1 });
							$('#content-page')
								.transition({   x: -20,  opacity: 0, delay: 500}, 0)
								.transition({   x: 0,   opacity: 1, height: "auto", delay: 150}, 1000);	
							$scope.$apply(); 
					};
					
					//	SET CURRENT
					$scope.setCurrent = function(entry){
						$scope.queryResults = entry; 

						// GET MOMENT
						$scope.entryDate = moment($scope.queryResults.submissionDate, "YYYYMMDD").fromNow();
							
						
						// CLEAN UP 
						$scope.queryResults.submitterEmail = custom.parseFromDatabase(entry.submitterEmail);
						$scope.queryResults.description = custom.parseFromDatabase(entry.description);
						$scope.queryResults.diety = custom.parseFromDatabase(entry.diety);
						$scope.queryResults.heaven = custom.parseFromDatabase(entry.heaven);
						$scope.queryResults.hell = custom.parseFromDatabase(entry.hell);
						

						if ( $.trim($scope.queryResults.image).length > 0){						
							$scope.logoImage = $location.host() + "../" +	$scope.queryResults.image;
						}
						else{
							$scope.logoImage  = "media/fallback/noPreview.png";
						}
						
						 										
						$scope.queryResults["followers2"] = custom.commaSeparateNumber(entry.followers); 
						$scope.queryResults["percentage"] = ((entry.followers/$scope.totalNumbers.population)*100).toFixed(0);
														
						var believers = custom.checkNumberType( $scope.totalNumbers.population * $scope.queryResults["percentage"] * .01 );
							var beliefPercent = $scope.queryResults["percentage"];  
						var nonBelievers = custom.checkNumberType( $scope.totalNumbers.population * ((100 - $scope.queryResults["percentage"]) * .01)  ); 
							var nonBeliefPercent = (100 - $scope.queryResults["percentage"]);
						var population = custom.checkNumberType( $scope.totalNumbers.population );						
						
						// check for small numbers
						if (believers.number <= 0){ believers = custom.checkNumberType($scope.queryResults["followers"]);   };
						if (beliefPercent <= 0){ beliefPercent = ".01"; }
						
						if (believers.number <= 0){ believers = custom.checkNumberType($scope.queryResults["followers"]);   };
						if (nonBeliefPercent >= 100){ nonBeliefPercent = "99.9"; }						
						
						
						$scope.worldStatistics = {
							believers: {numbers: believers.number,		unit: believers.unit, 		percentage: beliefPercent },
							nonBelievers: {numbers: nonBelievers.number,	unit: nonBelievers.unit, 	percentage: nonBeliefPercent },
							global: {numbers: population.number, 	unit: population.unit, inCommas: custom.commaSeparateNumber($scope.totalNumbers.population) }											
						};
						
						
						$location.search('type', $scope.queryResults.name);
						$scope.buildVisuals();				
						//custom.backstretchBG("http://lorempixel.com/1600/1200/abstract/6", 0, 1000);									
						$scope.$apply();
					};
					
					//	BUILD VISUALS
					$scope.buildVisuals = function(){
						$scope.donutData[1].name = $scope.queryResults.name;
						$scope.donutData[1].numbers =  parseInt($scope.queryResults.followers.replace(/,/g, ''), 10);						
					};

			        $scope.openTooltip = function openTooltip(data, id) {
			            $scope.donutDataTool = data;
			        };
			
			        $scope.closeTooltip = function closeTooltip(data, id) {
			           $scope.donutDataTool = {};
			        };						
					
					
					// WATCH FOR CHANGES
					$scope.searchObj = {};
					$scope.$watch('searchObj', function() {
						console.log($scope.searchObj)
						if ($scope.searchObj != null){
							if ($scope.searchObj.title != null){
								$scope.querySearch($scope.searchObj.title.toLowerCase());			      
							}
						}
					});	
			
					$scope.activateSearch = function(){
						$scope.querySearch($scope.selectObj.active.name.toLowerCase());
					};
					
					$scope.querySearch = function(name){
							var query = "SELECT * FROM db WHERE name='" +name+ "'"; 
							$scope.searchEntry(query, function(data){
								console.log(data)
								$scope.setCurrent(data[0]);	
							});	
					};
					
			
					// FETCH TYPEAHEAD 
					$scope.fetchTypeahead = function(callback){
					   	var packet = {
					   		query: "SELECT name, subname FROM db WHERE status='approved' ORDER BY ID",
					   		database: "codeandl_religion"
					   	};
					   	phpJS.queryDatabase(packet, function(state, data){
					   		if (state){							   				
					   			callback(data);		
					   		}							   		
					   	});								
					};					
					
					// SEARCH FOR ENTRY
					$scope.searchEntry = function(query, callback){
					   	$scope.isSearching = true;
					   	var packet = {
					   		query: query,
					   		database: "codeandl_religion"
					   	};						   
					   	phpJS.queryDatabase(packet, function(state, data){
					   		$scope.isSearching = false;
					   		if (state){		
					   			callback(data);
					   		}
					   	});											
					};
					
					// FETCH RANDOM ENTRY
					
					$scope.getRandomEntry = function(){
						$scope.isSearching = true;
						$scope.fetchRandomEntry(function(data){													
							$('#ex1_value').val(data[0].name);						
							$scope.setCurrent(data[0]);
							$scope.isSearching = false;	
							$scope.$apply();
						});
						
					};
					
					
					$scope.fetchRandomEntry = function(callback){
					   	var packet = {
					   		query: "SELECT COUNT(1) FROM db WHERE status='approved'",
					   		database: "codeandl_religion"
					   	};
					   	//--  get count of all entries
						phpJS.queryDatabase(packet, function(state, data){
							
							
							if (state){		
								$scope.totalEntries = data[0];													
								var randomEntry = custom.getRandomNumber(0, data[0] );
									//-- pick random entry and retrieve data
								   	var packet = {
								   		query: "SELECT * FROM db ORDER BY ID LIMIT " + randomEntry + ",1 ",
								   		database: "codeandl_religion"
								   	};
								   	phpJS.queryDatabase(packet, function(state, data){
								   		if (state){			   				
								   			callback(data);		
								   		}							   		
								   	});
							   		//--																
							}
						});
						//--
					};
								


				    // SLIDER
				    $scope.slideIndex = 0; 
					/* CUSTOM SLIDER */
					$scope.customIndex = 0;
					$scope.custom = [];
			        addSlides($scope.custom, 'technics', 10);						 					
		            $scope.cprev = function() {	
		                $scope.customIndex--;
		            };
		            $scope.cnext = function() {
		                $scope.customIndex++;
		            };
		            /* END CUSTOM */						
				
					/* SETUP */
		            function addSlide(target, style) {
		                var i = target.length;
		                target.push({
		                	id: (i + 1),
		                    label: 'Slide: ' + (i + 1),
		                    img: 'http://lorempixel.com/450/300/' + style + '/' + (i % 10) 
		                });
		            };
		
		            function addSlides(target, style, qty) {
		                for (var i=0; i < qty; i++) {
		                    addSlide(target, style);
		                }
		            }
					/* END SETUP */	
   

				});		
	    },
	    ///////////////////////////////////////
  };
});
