<div class="" style='' ng-init="init()">

	<!-- LOADING SCREEN -->
	<div style='top: 20%; position: absolute; width: 100%' ng-class="{true: 'hideMe'}[fullyLoaded]">
			<div ng-include src="'fragments/waiting/waveSpinner.html'"></div>
	</div>
	
	<!--  FOR CELLPHONES -->
	<div ng-if="browserDetails.mobile == true">		
		<div ng-include  src="'fragments/home/mobile.html'"></div>
	</div>
	
	
	<!--  FOR DESKTOPS -->
	<div ng-if="browserDetails.mobile == false">
		<div ng-include  src="'fragments/home/standard.html'"></div>	
	</div>
	<!-- END IF DESKTOP -->
		
</div>	

