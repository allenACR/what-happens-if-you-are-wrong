define(['custom', 'sharedData', 'firebaseSettings'], function(custom, sharedData, firebaseSettings) {

	
	var fileName  = 'page2';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($state, $scope, $timeout, cfpLoadingBar, toaster, browserInfo, $q, Linq, $state, $stateParams, $location, SmoothScroll) {	   
				    
				    // VARIABLES		
				    $scope.submissionComplete = false;		    
				    $scope.fullyLoaded = false;
				    $scope.userDetails = browserInfo.giveMeAllYouGot(); 
				    $scope.onState = 1; 
					$scope.pageData = {
						type: $stateParams.type,
						name: $stateParams.name
					};
					
					$scope.inputData = {
						status: 'Draft',
				    	id: null,
				    	name: null,
				    	subname: null,
				    	diety: null,
				    	description: null,
				    	year: null,
				    	followers: null,
				    	heaven: null,
				    	hell: null,
				    	image: null,
				    	submitterID: null				    					    
				    };
					
				    $scope.checkPass = {
				    	name: null,	
				    	subname: true,			    	
				    	year: null,
				    	description: null,
				    	diety: null,
				    	heaven: null,
				    	hell: null,
				    	followers: null
				    };
				    
					
				    $scope.checkBox = {
				    	showSub: false,
				    	date: false,
				    	diety: false,
				    	heaven: false,
				    	hell: false,
				    	followers: false,
				    	afterlife: true,
				    	core: true,				    				    	
				    };
				    

				    $scope.imagePreview = 'media/fallback/noPreview.png';
				    $scope.isBC = true;
				    
				    $scope.textareaCounter = {
				    	description: {total: 1500, counter: 0},
				    	diety: 		 {total: 1500, counter: 0},
				    	heaven: 	 {total: 1500, counter: 0},
				    	hell:	 	 {total: 1500, counter: 0}
				    };
				    $scope.siteComponents = [false, false, false];
				      
					$scope.queryResults = {
							relative: '',
							name: '',
							description: '',
							image: ''
					};
				    
				    $scope.froalaoptions = {
						placeholder : ''
					};
				        
					// INIT
					$scope.init = function(){
						SmoothScroll.$goTo(0);
						cfpLoadingBar.start();	
						custom.databaseInit(function(status){
							
							if (status){
								$scope.start();
							}
							else{
								alert("Timeout: Database could not be reached.");
							}
						});
						custom.backstretchBG("media/green-bg.jpg", 0, 1000);
																			 	
					};	
					
					// TRANSIT
					$scope.animatePage = function(){
							$('#content-page')
								.transition({   x: -20,  opacity: 0, delay: 500}, 0)
								.transition({   x: 0,   opacity: 1, height: "auto", delay: 150}, 1000);							
					};
					
					// START
					$scope.start = function(){
						$scope.checkLoad();
						
						$scope.fetchTypeahead(function(data){
							$scope.typeAhead = data;
							$scope.siteComponents[0] = true;
							$scope.checkLoad();
						});		
						phpJS.getIPAddress(function(state, data){
							$scope.ipAddress = JSON.parse(data).ipAddress;
							$scope.validateAll();
							$scope.siteComponents[1] = true;
							$scope.checkLoad();							
						});	 
						$scope.checkType(function(state, data){							
							$scope.siteComponents[2] = true;
							$scope.checkLoad();				
							
						});
						
							
					};
							
					// CHECK LOADED
					$scope.checkLoad =  function() {
						var isSiteLoaded = true;
						
						var i = $scope.siteComponents.length; while(i--){
							if ( $scope.siteComponents[i] == false){
								var isSiteLoaded = false; 
							}
						}
						if (isSiteLoaded){
							$scope.loadComplete();
						}					
					};	
					
					// LOAD COMPLETE
					$scope.loadComplete = function(){
							
							sharedData.request("user", function(state, data){
								if(state){
									$scope.userData = data.user;																	
								}
								else{
									$scope.userData = {
										id: null,
										email: null,
										firstName: null,
										lastname: null,
										permission: 'guest',										
										userName: 'anonymous'
									};
								}
								$scope.$apply();
							});
							
						
							cfpLoadingBar.complete();		
							$scope.animatePage();
							$scope.fullyLoaded = true; 							
							$scope.$apply();
							
							
					};		
					
					// SEARCH FOR ENTRY
					$scope.searchEntry = function(query, callback){
					   	$scope.isSearching = true;
					   	var packet = {
					   		query: query,
					   		database: "codeandl_religion"
					   	};						   
					   	phpJS.queryDatabase(packet, function(state, data){
					   		$scope.isSearching = false;
					   		if (state){		
					   			callback(data);
					   		}
					   	});											
					};
					
					$scope.editEntry = function(){
						if($scope.pageData.type == "edit"){
							$state.transitionTo($state.current, $stateParams, {
							    reload: true,
							    inherit: false,
							    notify: true
							});
						}
						else{
						 	var result = { type:'edit', name: $scope.inputData.name };
						 	$state.go('page2', result );
						 }
					};
										
								
					$scope.checkType = function(callback){
						
						// IF EDITING - LOAD DATA AND FORMAT
						if ($scope.pageData.type == "edit"){
							var query = "SELECT * FROM db WHERE name='" + $scope.pageData.name + "'"; 
							$scope.searchEntry(query, function(data){
								// no match - fetch random
								if (data.status == "error"){

								}
								// found, set 
								else{
									var data = data[0];
	
										$scope.inputData = {
											id: data.id,
											status: data.status,
									    	name: data.name,
									    	subname: data.subname,
									    	diety: data.diety,
									    	description: data.description,
									    	year: data.founding,
									    	followers: data.followers,
									    	heaven: data.heaven,
									    	hell: data.hell,
									    	image: data.image,	
									    	submitterID: data.submitterID		    	
									    };		
									    
									    						
										
										if ($scope.inputData.image != ''){
											$scope.imagePreview = $location.host() + "../" +  $scope.inputData.image;
										}
										
										// UNPARSE IF LOADING 
										$scope.queryResults = $scope.inputData;
										$scope.queryResults.submitterEmail = custom.parseFromDatabase($scope.inputData.submitterEmail);
										$scope.queryResults.description = custom.parseFromDatabase($scope.inputData.description);
										$scope.queryResults.diety = custom.parseFromDatabase($scope.inputData.diety);
										$scope.queryResults.heaven = custom.parseFromDatabase($scope.inputData.heaven);
										$scope.queryResults.hell = custom.parseFromDatabase($scope.inputData.hell);										

										//  OPEN IF NOT NULL
										if ($scope.queryResults.subname != null && $scope.queryResults.subname != ''){$scope.checkBox.showSub = true; };
										if ($scope.queryResults.year != null && $scope.queryResults.year != ''){$scope.checkBox.date = true; };
										if ($scope.queryResults.diety != null && $scope.queryResults.diety != ''){$scope.checkBox.diety = true; };
										if ($scope.queryResults.heaven != null && $scope.queryResults.heaven){$scope.checkBox.heaven = true; };
										if ($scope.queryResults.hell != null && $scope.queryResults.hell){$scope.checkBox.hell = true; };
										if ($scope.queryResults.followers != null && $scope.queryResults.followers){$scope.checkBox.followers = true; };

									callback(true);

								}
							});									
						}
						
						// ELSE CREATE
						if ($scope.pageData.type == "create"){
							callback(true);
						};
						
					};
					
					
					// TOASTER
					$scope.makeatoast = function(type, msg, subMsg){						
						toaster.pop(type, msg, subMsg);
					};		
					

					// FETCH TYPEAHEAD 
					$scope.fetchTypeahead = function(callback){
					   	var packet = {
					   		query: "SELECT name, subname FROM db ORDER BY ID",
					   		database: "codeandl_religion"
					   	};
					   	phpJS.queryDatabase(packet, function(state, data){
					   		if (state){							   											   			   		
					   			callback(data);		
					   		}							   		
					   	});								
					};	
	
					// VALIDATE
					$scope.checkAvailable = function(){
						
						$scope.inputData.name = $.trim($scope.inputData.name);
						// if creating
						if ($scope.pageData.type == "create"){
							if ($scope.inputData.name != null && $scope.inputData.name != ''){
								var name = $scope.inputData.name.toLowerCase(); 
								$q.all([
										Linq.From($scope.typeAhead),
										Linq.Where("(name == " + name + ")")
								]).then(function(results){
										data = results[1];
										if (data.length > 0){
											$scope.checkPass.name = false;
											$scope.checkPass.subname = false; 
											$scope.checkBox.showSub = true;
										}
										else{
											$scope.checkPass.name = true;
											$scope.checkPass.subname = true;
											$scope.checkBox.showSub = false;
											$scope.queryResults.name = $scope.inputData.name;
										}
								});						
							}
						}
						
						// if editing
						if ($scope.pageData.type == "edit"){
								$scope.checkPass.name = true;
								$scope.checkPass.subname = true;
						};
						
					};	
					
					$scope.checkSubNameAvailable = function(){
						$scope.inputData.subname = $.trim($scope.inputData.subname);
						
						if ($scope.pageData.type == "create"){
							if ($scope.inputData.subname == "" || $scope.inputData.subname == undefined){
								$scope.checkPass.subname = false;
								$scope.checkPass.name = false;
							}						
							if ($scope.inputData.subname != null){
								var name = $scope.inputData.subname.toLowerCase(); 
								var subname = $scope.inputData.subname.toLowerCase(); 
								$q.all([
										Linq.From($scope.typeAhead),
										Linq.Where("(name == " + name + ") && (subname == " + subname + ")")
								]).then(function(results){
										data = results[1];
										
										if (data.length > 0){
											$scope.checkPass.subname = false;
											$scope.checkPass.name = false;
										}
										else{
											$scope.checkPass.subname = true;
											$scope.checkPass.name = true;
											$scope.queryResults.subname = $scope.inputData.subname;
										}
								});						
							}
						}
						if ($scope.pageData.type == "edit"){
								$scope.checkPass.name = true;
								$scope.checkPass.subname = true;
						};
												
						
						
					};
					
					$scope.checkYear = function(){
						$scope.checkPass.year = null;
						if ($scope.pageData.type == "create"){
							if ($scope.inputData.year != null){
								var check = $scope.inputData.year.length;						
								if (check < 5 && check > 0){
									$scope.checkPass.year = true;
									$scope.queryResults.year = $scope.inputData.year;
								}
								else if (check == 0 || check == null){
									$scope.checkPass.year = null;
								}
								else{
									$scope.checkPass.year = false;
								};
							}
						}
						if ($scope.pageData.type == "edit"){
							$scope.checkPass.year = true;
						};						
						
					};
					
					$scope.checkFollowers = function(){
						$scope.checkPass.followers = null;
						if ($scope.inputData.followers != null){
							var check = $scope.inputData.followers.length;						
							if (check > 0){
								$scope.checkPass.followers = true;
								$scope.followerType = custom.checkNumberType(parseInt($scope.inputData.followers.replace(/,/g , "")));
								$scope.queryResults.followers = $scope.inputData.followers;
							}
							else if (check == 0 || check == null){
								$scope.checkPass.followers = null;
								$scope.followerType = null;
							}
							else{
								$scope.checkPass.followers = false;
								$scope.followerType = null;
							};
						}
					};					
					
					$scope.checkTextareas = function(type){	
						$scope.checkPass[type] = null;										
						if ($scope.inputData[type] != null){
							
							descriptionLength = $scope.inputData[type].replace(/<\/?[^>]+(>|$)/g, "").length;
							
							$scope.textareaCounter[type].counter = $scope.textareaCounter[type].total - descriptionLength;
							$scope.queryResults[type] = $scope.inputData[type];
							
							if (descriptionLength < 1500 && descriptionLength > 0){
								$scope.checkPass[type] = true;
							}
							else if (descriptionLength == 0 || descriptionLength == null){
								$scope.checkPass[type] = null;
							}
							else{
								$scope.checkPass[type] = false;
							};
						}
						else{
							$scope.textareaCounter[type].counter = 1500;
						}						
					};		
					
					
	
					// IMAGE PREVIEW 
					$("#uploadSource").change(function(){
						readURL(this, function(data){						
							$scope.imagePreview = data; 
							$scope.$apply();
						});
					});		
					function readURL(input, callback) {
					    if (input.files && input.files[0]) {
					        var reader = new FileReader();
					        reader.onload = function (e) {
					           callback(e.target.result);								
					        };
								
							var type = input.files[0].type; 	
							if (type.indexOf('image') > -1) {
							  	reader.readAsDataURL(input.files[0]);
							} else {
							 	alert("That file is not an image.  Please select another file.");
							 	$("#uploadSource").val(null);
							}							       
					    }
					}		
					$scope.clearImage = function(){
						$("#uploadSource").val(null);
						$scope.imagePreview = 'media/fallback/noPreview.png';
					};
					
					
				   	////////////////////
					// TEXT EDITOR
					$scope.froalaAction = function(action){
						$scope.options.froala(action);
					};
			
					$scope.onPaste = function(e, editor, html){
						
					};
			
					$scope.onEvent = function(e, editor){
						
					};
					////////////////////
					
					$scope.validateAll = function(){
					
						$scope.checkAvailable();	
					    if ($scope.checkBox.showSub){
					    	$scope.checkSubNameAvailable();	
					    }							
						$scope.checkYear();
						$scope.checkFollowers();
						$scope.checkTextareas("description");
						$scope.checkTextareas("diety");		
						$scope.checkTextareas("heaven");
						$scope.checkTextareas("hell");													
					};
					
					
					$scope.readyToSubmit = function(){
						
						// validate all
						$scope.validateAll();
						
						function checkNulls(field){
							if (field == null || field == undefined){
								field = "";
							}
							field = "\"" + field + "\"";	
							return field;					   						
						}							
						
						// bypass keypass if checkboxes are false
					    if ($scope.checkBox.subname == false){
					    	$scope.checkPass.subname = null;
					    }							
					    if ($scope.checkBox.date == false){
					    	$scope.checkPass.year = true;
					    }	
					    if ($scope.checkBox.diety == false){
					    	$scope.checkPass.diety = true;
					    }		
					    if ($scope.checkBox.heaven == false){
					    	$scope.checkPass.heaven = true;
					    }	
					    if ($scope.checkBox.hell == false){
					    	$scope.checkPass.hell = true;
					    }	
					    if ($scope.checkBox.followers == false){
					    	$scope.checkPass.followers = true;
					    }						    					
						
						// passcheck 
						var p = $scope.checkPass; 
						passCheck = true;
						for (var key in p) {
						  if (p.hasOwnProperty(key)) {
						    if (p[key] != true){
						    	passCheck = false;
						    }
						    //console.log(p[key], key)
						  }
						}
						
						// RESET FIELDS
						$scope.validateAll();
						
						// IF VALID
						if (passCheck){
								
							
							// CHECK FOR IMAGE							
							var file = $('#uploadSource')[0].files[0];
							if (file != null || file != undefined){	
								phpJS.basicUpload(file, "logo", "images/" + $scope.inputData.name.toLowerCase(), function(state, data){
									if (state){
										imagePath = data.relativePath;
										uploadEntry();
									}	
									else{
										alert("Error uploading image.  Please try again.");
									}																						
								});							
							}	
							else{	
								// CREATE NO IMAGE
								if ($scope.pageData.type == "create"){
									imagePath = '';							
								}			
								// EDITING NO CHANGES (keep old image)	
								if ($scope.pageData.type == "edit"){
									imagePath = $scope.inputData.image;
								}
								uploadEntry();
							}					
						
								
							function uploadEntry(){						
								
								
								function wrapText(field, allLower){
									if (field == null || field == undefined){
										field = "";
									}
									field = "\"" + field + "\"";
									if (allLower){
										field = field.toLowerCase();	
									}	
									return field;					   						
								}


								var packet = {
									founding: wrapText($scope.inputData.year, false),
									followers: wrapText($scope.inputData.followers, false),
									name: wrapText($scope.inputData.name, true),
									subname: wrapText($scope.inputData.subname, true),
									description: wrapText(custom.parseForDatabase($scope.inputData.description), false),
									diety: wrapText(custom.parseForDatabase($scope.inputData.diety), false),
									heaven: wrapText(custom.parseForDatabase($scope.inputData.heaven), false),
									hell: wrapText(custom.parseForDatabase($scope.inputData.hell), false),
									image: wrapText(imagePath, false),
									submitter: wrapText($scope.userData.userName, false),
									submitterType: wrapText($scope.userData.permission, false),
									submitterID: $scope.userData.id,
									submitterIp: wrapText($scope.ipAddress, false) 
								};
								
								// INHERIT STATUS IF 
								if ($scope.pageData.type == "create"){
									packet.status = wrapText("created", true);
								}
								// INHERIT STATUS IF 
								if ($scope.pageData.type == "edit"){
									packet.status = wrapText("edited", true);
								}
								
								var dbpacket = {
							 		database: 'codeandl_religion', 
							 		table: "db"
							 	};									
										
						 		var submitPacket = JSON.stringify(packet);	
						 		
						 		function createNewEntry(){
									phpJS.createNewEntry(dbpacket, submitPacket, function(state, data){	
										if (data.status == "error"){
											$scope.makeatoast('warning', "Could not upload to database...", 'Submission Error.');
				 							$scope.$apply();
										}
										else{
				 							if (state){				
				 								SmoothScroll.$goTo(0); 								
				 								$scope.submissionComplete = true;
												$scope.newlyCreated = data;												
												$scope.makeatoast('success', "Entry submitted.  Will appear regularly once it has been verified.", 'Thank you for the submission!');
												$scope.$apply();
				 							}
				 							else{
				 								$scope.makeatoast('warning', "Could not upload to database...", 'Please try again later.');
				 								$scope.$apply();
				 							}		
			 							}			
									});						 			
						 		}
						 		
						 		function deleteEntry(callback){
								   	var packet = {
								   		query: "DELETE FROM " + dbpacket.table + " WHERE id=" + $scope.inputData.id,
								   		database: dbpacket.database
								   	};
								   	

									phpJS.queryDatabase2(packet, function(state, data){
										if (state){											
											callback();
										}
									});		
								
						 		}
						 		
						 		
						 		// CREATE NEW ENTRY
						 		if ($scope.pageData.type == "create"){
									createNewEntry();
								}
								
								// EDITING AN EXISTING ENTRY
								 //check to make sure ID'S match one last time
								sharedData.request("user", function(state, data){
									if(state){			
					
										if (data.user.id == $scope.inputData.submitterID ||   		// if author is also the submitter 
											data.access.permission == "superadmin" ||				// if author is superadmin
											$scope.inputData.submitterID == '' || 					// or if submitter is anoynomous
											$scope.inputData.submitterID == null){					// of it submitter is null
												
												// if accepted, then continue
												if ($scope.pageData.type == "edit"){
													deleteEntry(function(){
														createNewEntry();
													});
												}									
										}
										else{
											alert("You are not authorized to edit this entry.");
										}									
									}
									else{
										alert("Problem retrieving user information.  Please logout and log back in and try again.");
									}									
								});
								
								
								
								
								
							}
								
						}
						else{
							alert("All fields are not complete.");
							
						}
						
									
					};
					

					
				

				
	    
				});
						
	    },
	    ///////////////////////////////////////
  };
});
