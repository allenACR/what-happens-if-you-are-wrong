<div class="" style='min-height: 1200px' ng-init="init()">

	<!-- LOADING SCREEN -->
	<div style='top: 20%; position: absolute; width: 100%' ng-class="{true: 'hideMe'}[fullyLoaded]">
			<div ng-include src="'fragments/waiting/waveSpinner.html'"></div>
	</div>

	<!-- CONTENT -->
	<div id='content-page' ng-class="{false: 'hideMe'}[fullyLoaded]" style='background-color: rgba(0, 152, 107, .8); padding-top: 50px; padding-bottom: 100px;'>

	<div class="row" ng-if="submissionComplete == true">
		<div class="small-12 columns">
			<h1>SUBMISSION COMPLETE</h1>
			<p>Thank you for contributing!  You will be notified when your entry has been approved!</p>
			<br><br>
			<centered>
			  
			  <button class='button alert' ng-click="editEntry()">Edit {{inputData.name}} </button>
			  <br>
			  <button class='button  small-4' ui-sref="home">Home</button>
			  <br>
			  <button class='button small-4' ui-sref="page2">Submit another entry</button>
			  <br>
			  <button class='button small-4' ui-sref="admin">My Account</button>
			  
			</centered>
			
		</div>
	</div>

	<div  ng-if="submissionComplete == false">
		<div class="row">
			<div class="small-12 columns">
					
					<div ng-if="pageData.type == 'create'"><h1>SUBMIT AN ENTRY</h1></div>
					<div ng-if="pageData.type == 'edit'"><h1>EDIT {{pageData.name}}</h1></div>	
								
					<br>
					<!-- {{accessLevel}} -->
					<div ng-if="userData.permission == 'guest'">
						<h5>
							* You are currently not a member of this site.  You can <strong>still submit</strong>, but 
							any regular members will be able to edit your entries.  If you'd like to safeguard your
							contributions, join as a member first (free of course).  						
						</h5>
					</div>
			</div>		
		</div>
		<hr>	
		
		<div class="row">
			<div class='small-12 columns'>
			
				<dl class="tabs">
				  <dd class='small-3' ng-click='onState = 1' ng-class="{false: 'active'}[onState == 1]">
				  	<a>
				  		<center> 
				  			<span ng-class="{false: 'hidden'}[onState == 1]"><i class="fa fa-dot-circle-o"></i></span> 
				  			<span class='fonttype-2'>Name</span>
				  		</center>
				  	</a>
				  </dd>
				  <dd class='small-3' ng-click='onState = 2' ng-class="{false: 'active'}[onState == 2]">
				  	<a>
				  		<center> 
				  			<span ng-class="{false: 'hidden'}[onState == 2]"><i class="fa fa-dot-circle-o"></i></span> 
				  			<span class='fonttype-2'>Details</span>
				  		</center>
				  	</a>
				  </dd>
				  <dd class='small-3' ng-click='onState = 3' ng-class="{false: 'active'}[onState == 3]">
				  	<a>
				  		<center> 
				  			<span ng-class="{false: 'hidden'}[onState == 3]"><i class="fa fa-dot-circle-o"></i></span> 
				  			<span class='fonttype-2'>Image</span>
				  		</center>
				  	</a>
				  </dd>
				  <dd class='small-3' ng-click='onState = 4' ng-class="{false: 'active'}[onState == 4]">
				  	<a>
				  		<center> 
				  			<span ng-class="{false: 'hidden'}[onState == 4]"><i class="fa fa-dot-circle-o"></i></span> 
				  			<span class='fonttype-2'>Preview</span>
				  		</center>
				  	</a>
				  </dd>
				</dl>	
			
			</div>
		</div>
		
		
		<div class="row" ng-class="{false: 'hidden'}[onState == 1]">
			<div class="small-12 columns">
					
					
					
					<!-- GATHER -->
					<div class="small-12 medium-12 pull-left columns">
						<br><br>
						<h3>BASIC INFORMATION</h3>
						
						
						
						
						<!-- name -->	
						<div class="small-12 medium-6 pull-left">
							<div class="small-12 pull-left">
								<div class="small-12 pull-left" style=''>
									<label>Name of the religious institution and/or denomination:</label><br>
								</div>
								<div class="small-11 pull-left" style=''>
									<input ng-change='checkAvailable()' placeholder='Text, numbers, and select special characters allowed.' class='inputFitting' ng-model='inputData.name'  type="text" ng-pattern-restrict="^[A-Za-z0-9'() ]*$" />
								</div>
								<div class='small-1 pull-left'>
									<span class='img-responsive inputFitting' ng-class='{false: "warning-color", true: "success-color", null: "info-color"}[checkPass.name]'>
										<centered>
											<i class="fa" ng-class="{false: 'fa-thumbs-o-down', true: 'fa-thumbs-o-up', null: 'fa-exclamation-triangle'}[checkPass.name]"></i>
										</centered>
									</span>		
								</div>					
							</div>
						
							<div class="small-12  pull-left" ng-class='{true: "hidden"}[checkBox.showSub == false]'>
								<div class="small-12 pull-left" style=''>
									<label><strong>That entry already exists.</strong> If this is a denomination or a sub group, enter it here: </label><br>
								</div>
								<div class="small-11 pull-left" style=''>
									<input ng-change='checkSubNameAvailable()'  placeholder='Text, numbers, and select special characters allowed.' class='inputFitting' ng-model='inputData.subname'  type="text" ng-pattern-restrict="^[A-Za-z0-9'() ]*$" />
								</div>
								<div class='small-1 pull-left'>
									<span class='img-responsive inputFitting' ng-class='{false: "warning-color", true: "success-color", null: "info-color"}[checkPass.name]'>
										<centered>
											<i class="fa" ng-class="{false: 'fa-thumbs-o-down', true: 'fa-thumbs-o-up', null: 'fa-exclamation-triangle'}[checkPass.name]"></i>
										</centered>
									</span>		
								</div>							
							</div>
						</div>
						
						<gap></gap>
						
						<div class="small-12 pull-left">
							<label>Summary of the religion: ({{textareaCounter.description.counter}} words left)</label><br>
							<div class="small-12" style='background-color: white; color: black'>
								<textarea froala="froalaoptions" ng-model="inputData.description" ng-change="checkTextareas('description', 'description')" froala-event-on-paste="onPaste" froala-event-focus="onEvent" froala-event-indent="onEvent" placeholder='Description'></textarea>
							</div>
							<gap></gap>
						</div>
						
						
					</div>
					<!-- END GATHER -->
	
	
					
							
	
			</div>	
		</div>	
		
		<div class="row" ng-class="{false: 'hidden'}[onState == 2]">
			<div class="small-12 columns pull-left">
					<br><br>
					<h3>DETAILS</h3>				
					<div class='small-12 pull-left'>
						<span ng-model="checkBox.date" btn-checkbox btn-checkbox-true=true btn-checkbox-false=false>
		        			<p>Submission Status: &nbsp;<em>{{inputData.status | stringCapitalize}}</em></p>
		    			</span>
	    			</div>
					
					<!-- approximate date -->
					<div class='small-12 pull-left'>
						<span ng-model="checkBox.date" btn-checkbox btn-checkbox-true=true btn-checkbox-false=false>
		        			<p><i class="fa" ng-class="{true: 'fa-check-square', false: 'fa-square'}[checkBox.date]"></i> I know the year this religion was founded.</p>
		    			</span>
	    			</div>
		 
					<div class='small-12 pull-left' ng-class='{true: "reveal", false: "vanish"}[checkBox.date]'>
						<div class="small-12 pull-left" style=''>
							<label>Approximate date of founding:</label><br>
						</div>
						<div class="small-8 medium-3 pull-left" style=''>
							<input ng-change='checkYear()' placeholder='Format: ####' class='inputFitting' ng-model='inputData.year' type="text" ng-pattern-restrict="^[0-9\b]{0,4}$" />
								
						</div>
	
						<div class="small-2 medium-1 pull-left">
						  	<span ng-click='isBC = !isBC' class='gray-gradient img-responsive inputFitting isPointer fonttype-2 black-font' >
								<centered>
									<span ng-class='{false: "hidden"}[isBC]'>C.E</span>
									<span ng-class='{true: "hidden"}[isBC]'>B.C.E</span>
								</centered>
							</span>	
						</div>						
					
					
						<div class='small-2 medium-1 pull-left'>
							<span class='img-responsive inputFitting' ng-class='{false: "warning-color", true: "success-color", null: "info-color"}[checkPass.year]'>
								<centered>
									<i class="fa" ng-class="{false: 'fa-thumbs-o-down', true: 'fa-thumbs-o-up', null: 'fa-exclamation-triangle'}[checkPass.year]"></i>
								</centered>
							</span>		
						</div>	
						
					<gap></gap>
					</div>				
					
					<!-- approximate numbers -->
					<div class='small-12 pull-left'>
						<span ng-model="checkBox.followers" btn-checkbox btn-checkbox-true=true btn-checkbox-false=false>
		        			<p><i class="fa" ng-class="{true: 'fa-check-square', false: 'fa-square'}[checkBox.followers]"></i> I know how many believers follow this particular belief set.</p>
		    			</span>
	    			</div>
		 
					<div class='small-12 pull-left' ng-class='{true: "reveal", false: "vanish"}[checkBox.followers]'>
						<div class="small-12 pull-left" style=''>
							<label>Approximate number of followers (commas optional):</label><br>
						</div>
						<div class="small-8 medium-4 pull-left" style=''>
							<input ng-change='checkFollowers()' placeholder='Format: ###,###,###,###' class='inputFitting' ng-model='inputData.followers' type="text" ng-pattern-restrict="^[0-9,\b]{0,15}$" />
							<p ng-show="followerType.number != null"><strong>~ {{followerType.number}} {{followerType.unit}} followers</strong></p>
						</div>
	
						<div class='small-2 medium-1 pull-left'>
							<span class='img-responsive inputFitting' ng-class='{false: "warning-color", true: "success-color", null: "info-color"}[checkPass.year]'>
								<centered>
									<i class="fa" ng-class="{false: 'fa-thumbs-o-down', true: 'fa-thumbs-o-up', null: 'fa-exclamation-triangle'}[checkPass.year]"></i>
								</centered>
							</span>		
						</div>	
						<gap></gap>
					</div>	
					
					
					<!-- basic description -->
					<div class='small-12 pull-left'>
						<span ng-model="checkBox.diety" btn-checkbox btn-checkbox-true=true btn-checkbox-false=false>
		        			<p><i class="fa" ng-class="{true: 'fa-check-square', false: 'fa-square'}[checkBox.diety]"></i> This religion has a diety or dieties.</p>
		    			</span>
	    			</div>				
					<div class='small-12 pull-left' ng-class='{true: "reveal", false: "vanish"}[checkBox.diety]'>
						
							<label>Description of Diety/Dieties: ({{textareaCounter.diety.counter}} words left)</label>
						
						<div class="small-12" style='background-color: white; color: black'>
							<textarea froala="froalaoptions" ng-model="inputData.diety" ng-change="checkTextareas('diety', 'diety')" froala-event-on-paste="onPaste" froala-event-focus="onEvent" froala-event-indent="onEvent"></textarea>
						</div>
						<gap></gap>
					</div>	
					
					<!-- heaven -->
					<div class='small-12 pull-left'>
						<span ng-model="checkBox.heaven" btn-checkbox btn-checkbox-true=true btn-checkbox-false=false>
		        			<p><i class="fa" ng-class="{true: 'fa-check-square', false: 'fa-square'}[checkBox.heaven]"></i> This religion has a version of <strong>heaven</strong> in it's afterlife.</p>
		    			</span>
	    			</div>		
	    			<div class='small-12 pull-left' ng-class='{true: "reveal", false: "vanish"}[checkBox.heaven]'>			
						
							<label>Description of Heaven: ({{textareaCounter.heaven.counter}} words left)</label>
							<p class='pull-right'>help?</p>
						
						<div class="small-12" style='background-color: white; color: black'>
							<textarea froala="froalaoptions" ng-model="inputData.heaven" ng-change="checkTextareas('heaven', 'heaven')" froala-event-on-paste="onPaste" froala-event-focus="onEvent" froala-event-indent="onEvent"></textarea>
						</div>
						<gap></gap>
					</div>
					
					<!-- heaven -->
					<div class='small-12 pull-left'>
						<span ng-model="checkBox.hell" btn-checkbox btn-checkbox-true=true btn-checkbox-false=false>
		        			<p><i class="fa" ng-class="{true: 'fa-check-square', false: 'fa-square'}[checkBox.hell]"></i> This religion has a version of <strong>hell</strong> in it's afterlife.</p>
		    			</span>
	    			</div>		
	    			<div class='small-12 pull-left' ng-class='{true: "reveal", false: "vanish"}[checkBox.hell]'>	
	    				
							<label>Description of Hell: ({{textareaCounter.hell.counter}} words left)</label>
						
						<div class="small-12" style='background-color: white; color: black'>
							<textarea froala="froalaoptions" ng-model="inputData.hell" ng-change="checkTextareas('hell', 'hell')"  froala-event-on-paste="onPaste" froala-event-focus="onEvent" froala-event-indent="onEvent"></textarea>
						</div>
						<br>
					</div>
			
			</div>	
		</div>	
		
		<div class="row" ng-class="{false: 'hidden'}[onState == 3]">
			<div class="small-12 columns">
					<br><br>
					<h3>IMAGE UPLOAD</h3>				
					<br>				
			</div>	
			<div class="small-6 medium-8 columns pull-left">
					<form id='uploadForm' enctype="multipart/form-data">
					    Select image to upload:
					    <input id='uploadSource' type="file">
					</form>	
						
			</div>
			<div class="small-6 medium-3 columns pull-right">
					<img ng-src='{{imagePreview}}' class='img-responsive'/>	
					<button class='img-responsive' ng-click='clearImage()'>Clear Image</button>
			</div>		
			
		</div>			
		
		<div class="row" ng-class="{false: 'hidden'}[onState == 4]">
			<div class="small-12 columns">
					<br><br>
					<h3>PREVIEW</h3>				
					<br>
					
					<!-- INFO -->
					<div ng-include src="'fragments/content/info.html'"></div>
	
					<button class='img-responsive' ng-click='readyToSubmit()'>Submit</button>
					
			</div>	
		</div>	
		</div>	
	</div>	
   
   
   	<!-- GAP -->
	<gap></gap>
	<gap></gap>
	<gap></gap>
	<gap></gap>


</div>	

