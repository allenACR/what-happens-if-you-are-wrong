define(['custom', 'papaParse'], function(custom) {

	
	var fileName  = 'page3';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, cfpLoadingBar) {	   
				    
					// INIT
					$scope.init = function(){
						custom.transitIn();		
						cfpLoadingBar.start();	
						custom.backstretchBG("http://lorempixel.com/1600/1200/abstract/", 0, 1000);					 
					};	
							
				    // fake the initial load so first time users can see it right away:
				    $timeout(function() {
				      cfpLoadingBar.complete();
				    }, 750);
				   

				});
						
	    },
	    ///////////////////////////////////////
  };
});
