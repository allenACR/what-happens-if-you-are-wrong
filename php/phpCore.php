<?php


require 'settings.php';

//  PARSE THE CALL	
//
if(isset($_POST['action']) && !empty($_POST['action'])) {

    $action 			= $_POST['action'];

    switch($action) {
		case 'testFunction'			: testFunction();break;
		case 'getPHPStatus'			: getPHPStatus();break;
		
		case 'getIPAddress'			: getIPAddress(); break;
		case 'createNewEntry'		: createNewEntry($_POST['database'], $_POST['table'], $_POST['values']); break;
		case 'getTableFields'		: getTableFields($_POST['database'], $_POST['table']); break;
		case 'queryDatabase'		: queryDatabase($_POST['database'], $_POST['query']);break;
		case 'queryDatabase2'		: queryDatabase2($_POST['database'], $_POST['query']);break;
		case 'sendEmail'			: sendEmail($_POST['sendTo'], $_POST['subject'], $_POST['content'], $_POST['replyTo']);break;
		case 'basicUpload'			: basicUpload($_POST['base64'], $_POST['nameOfFile'], $_POST['fileLocation']); break;
    }
}	
//
///////////


		////////////////////////
		//
		function testFunction(){
			
			echo "PHP Test script successful.";		

		}
		//
		////////////////////////
		
		////////////////////////
		//
		function getIPAddress(){
				$fileData = array(
						"ipAddress" => get_client_ip()						
				);	
				echo json_encode($fileData);				
		}
		//
		////////////////////////
		

		////////////////////////
		//	
		function createNewEntry($database, $dbTable, $values){
			
			global $link; 
			
			if(connectToDatabase($database) ){
				
				$db_selected = mysql_select_db($database);  // 											
				$values = json_decode($values, true);	
				
				//CONVERT ARRAY TO TEXT
				reset($values);
				$keys = array();
				$vals = array();
				while (list($key, $val) = each($values)) {
				    //echo "$key => $val\n";
					$keys[] = "$key";
					$vals[] = "$val";
				}
				$keyString = implode(',', $keys);
				$valueString = implode(',', $vals);
				
				// INSERT INTO DATABASE
				$sql = 	"INSERT INTO $dbTable" . 
					 	"(" . $keyString . ")" . 
						"VALUES (". $valueString . ")";
				if (mysql_query( $sql, $link )){
					$fileData = array(
						"status" => "good",
						"createdId" => mysql_insert_id()
					);	
					echo json_encode($fileData);	
				}
				else{
					echo returnFalse();	
				}
			}		
		}
		//
		////////////////////////
				
		////////////////////////
		//	
		function getTableFields($database, $dbTable){
			
			
			if(connectToDatabase($database) ){
								
				// SELECT THE BLOG DATABASE
				$db_selected = mysql_select_db($database);  // 			
				$result = mysql_query("SHOW COLUMNS FROM $dbTable");
				
				if (!$result) {
				   	returnFalse();
				    exit;
				}
				if (mysql_num_rows($result) > 0) {
					$resultArray = array();
				    while ($row = mysql_fetch_assoc($result)) {
				       $resultArray[] = $row;
				    };
					
					echo json_encode($resultArray);
				}
				
			}
			
		}
		//
		////////////////////////
		
				
		////////////////////////
		//
		function getPHPStatus(){
			
			global $link;
			global $status; 
				 
			/*  DATABASES */	
			$dbExample		= 'codeandl_example';								// DATABASE 1
			$dbReligion		= 'codeandl_religion';								// DATABASE 2

	
				/* CHECK DATABASE	 */
				if ($link != false){
			
					if(connectToDatabase($dbExample) ){				
						$dbExampleStatus = "true";		
					}
					else{
						$dbExampleStatus = "false";
					}	

					if(connectToDatabase($dbReligion) ){				
						$dbReligionStatus = "true";		
					}
					else{
						$dbReligionStatus = "false";
					}								
					
			
				};
				
				
				$fileData = array(
				    "status" => "$status",
				    "connectionType" => "$link",			   
				    "codeandl_example" => "$dbExampleStatus",
				    "codeandl_religion" => "$dbReligionStatus"
				);			
				echo json_encode($fileData);				
				
		}
		//
		////////////////////////		

		
		////////////////////////
		//
		function queryDatabase2($database, $query){

			global $link;

			if(connectToDatabase($database) ){
								
				// SELECT THE BLOG DATABASE
				
				$db_selected = mysql_select_db($database);  // 
				if (mysql_query( $query, $link )){
					returnTrue();
					exit;
				}
				else{
					returnFalse();
					exit;
				}	
	

			}
			else{
				returnFalse();
				exit;
			}	
			
		}		
		//
		////////////////////////
		

				
		////////////////////////
		//
		function queryDatabase($database, $query){

			if(connectToDatabase($database) ){
								
				// SELECT THE BLOG DATABASE
				$db_selected = mysql_select_db($database);  // 
				
						// DO QUERY  
						$json = sql2json($query);
						// QUERY IS VALID
						if ($json != ''){
							echo $json;
						}				
						// QUERY IS INVALID		
						else{
							returnFalse();
							exit;
						};

			}
			else{
				returnTrue();
				exit;
			}	
			 
			
		}
		//
		////////////////////////		
		
		
		////////////////////////
		//
		function basicUpload($base64, $nameOfFile, $fileLocation){
			
			// read base64 image 
			list($type, $base64) = explode(';', $base64);
			list(, $base64)      = explode(',', $base64);
			$base64 = base64_decode($base64);	
						
			// image data	
			$fileName = $nameOfFile;
			$extension = ".png"; 						
			$directory = '../uploads/' . $fileLocation . '/';
			$fileLocation = $directory . $fileName . $extension;
			
			// create directory if it does not exist
			$dirCheck = is_dir($directory);
			if(!$dirCheck){
				if (!mkdir($directory, 0777, true)) {
				    die('Failed to create folders...');
				}				
			}
			
			// place into location on server
			file_put_contents($fileLocation, $base64);
			
			// get image
			$dimensions = getimagesize($fileLocation);
			$size = filesize($fileLocation);
			$absPath = realpath($fileLocation);
			
			// return an array with image data
			$fileData = array(
			    "name" => $fileName,
			    "ext" => $extension,			   
			    "dimensions" => $dimensions,
			    "size" => $size,
			    "relativePath" => $fileLocation,
			    "absolutePath" => $absPath
			);			
			echo json_encode($fileData);
	
		}
		//
		////////////////////////		
		
		
		
		////////////////////////
		//	
		function sendEmail($sendTo, $subject, $content, $replyTo){
			
			$message	= 	"<!DOCTYPE html><html><head></head><body>" . 
							"<div>" .
							$content . 
							"</div>" .
							"</body></html>";
			
			$headers = "From: " . strip_tags($replyTo) . "\r\n";
			$headers .= "Reply-To: ". strip_tags($replyTo) . "\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			
			@mail($sendTo,$subject,$message,$headers);

			$fileData = array(
			    "status" => "sent",
			    "sendTo" => $sendTo,
			    "subject" => $subject,			   
			    "replyTo" => $replyTo,
			);			
			echo json_encode($fileData);

			
		}
		//
		////////////////////////

?>