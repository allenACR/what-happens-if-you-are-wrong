<?php

	/*  CONNECTION INFORMATION */
	$remoteName = 'just65.justhost.com';
	$localName = 'localhost';	
	$link = '';
	$name	= 'codeandl_admin';							// LOGIN NAME
	$key	= 'E^mLjf&8mV1r';								// PASSWORD  (This must match the one in JUSTHOST.com)	
	$status = '';
	$connection = 'true';
	$val_to_print 	= "";
	
	/* CHECK FOR LOCAL OR SERVER */
	if(@mysql_connect($remoteName, $name , $key)){
		$status = 'remote';
		$connection = 'true';
		$link   = @mysql_connect($remoteName, $name , $key);
	}
	else if(@mysql_connect($localName, $name , $key)){
		$status = 'server';
		$connection = 'true';
		$link   = @mysql_connect($localName, $name , $key);
	}
	else{
		$status = 'Failed to connect';
		$connection = 'false';
	};
	
	
	////////////////////////
	//  CHECK FUNCTIONS
	function connectToDatabase( $dbType ){
		
			global $val_to_print;
			global $link;
			global $connectTo;
			global $link;  
			global $name; 
			global $key; 
			// ENSURE LINK TO SERVER IS GOOD
			
				if (!$link) {
					$connectTo = $connection;
					$link   = @mysql_connect($connectTo, $name , $key);
				}	
			//
		  
			// ENSURE LINK TO SERVER IS GOOD
				if (!$link) {
					$val_to_print .= ('Could not establish link.  ');
					return false;
				}
			//
			
			// CHECK INDIVIDUAL DATABASES
				$db_selected = mysql_select_db($dbType);
				if (!$db_selected) {
				  $val_to_print .= ('Could not connect to blog database.  ');
				  return false;
				}
			// 
			
			// IF ALL IS GOOD, RETURN TRUE
			return true; 
			
	}
	//
	////////////////////////
	
	////////////////////////
	//
	function connectionStatus( $dbType ){
		
			global $val_to_print;
			
			if( connectToDatabase( $dbType ) ){				
				$val_to_print .= ('All databases connected successfully.');
				echo ($val_to_print);
			}	
			else{
				$val_to_print .= ('Error in connecting to the database structure.');
				echo ($val_to_print);
			};
	}
	//
	////////////////////////
	
	
	////////////////////////
	//	
	function returnFalse(){
		$fileData = array(
		    "status" => "error",
		);	
		echo json_encode($fileData);
	}
	//
	////////////////////////	
	
	
	////////////////////////
	//	
	function returnTrue(){
		$fileData = array(
		    "status" => "good",
		);	
		echo json_encode($fileData);
	}
	//
	////////////////////////		

	////////////////////////
	//	
	function get_client_ip() {
		
		// check for shared internet/ISP IP
		if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
			return $_SERVER['HTTP_CLIENT_IP'];
		}
	
		// check for IPs passing through proxies
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			// check if multiple ips exist in var
			if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
				$iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
				foreach ($iplist as $ip) {
					if (validate_ip($ip))
						return $ip;
				}
			} else {
				if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR']))
					return $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
		}
		if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED']))
			return $_SERVER['HTTP_X_FORWARDED'];
		if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
			return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
		if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
			return $_SERVER['HTTP_FORWARDED_FOR'];
		if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED']))
			return $_SERVER['HTTP_FORWARDED'];
	
		// return unreliable ip since all else failed
		return $_SERVER['REMOTE_ADDR'];
 	
	}	

	function validate_ip($ip) {
		if (strtolower($ip) === 'unknown')
			return false;
	
		// generate ipv4 network address
		$ip = ip2long($ip);
	
		// if the ip is set and not equivalent to 255.255.255.255
		if ($ip !== false && $ip !== -1) {
			// make sure to get unsigned long representation of ip
			// due to discrepancies between 32 and 64 bit OSes and
			// signed numbers (ints default to signed in PHP)
			$ip = sprintf('%u', $ip);
			// do private network range checking
			if ($ip >= 0 && $ip <= 50331647) return false;
			if ($ip >= 167772160 && $ip <= 184549375) return false;
			if ($ip >= 2130706432 && $ip <= 2147483647) return false;
			if ($ip >= 2851995648 && $ip <= 2852061183) return false;
			if ($ip >= 2886729728 && $ip <= 2887778303) return false;
			if ($ip >= 3221225984 && $ip <= 3221226239) return false;
			if ($ip >= 3232235520 && $ip <= 3232301055) return false;
			if ($ip >= 4294967040) return false;
		}
		return true;
	}
	//
	////////////////////////	
	
	////////////////////////
	//
	function sql2json($query) {
	    $data_sql = mysql_query($query);// If an error has occurred,
	    
	    if (!$data_sql) { 
	         return false;
		}
		else{
	    $json_str = ""; //Init the JSON string.
	    if($total = mysql_num_rows($data_sql)) { //See if there is anything in the query
	        $json_str .= "[\n";
	
	        $row_count = 0;    
	        while($data = mysql_fetch_assoc($data_sql)) {
	            if(count($data) > 1) $json_str .= "{\n";
	
	            $count = 0;
	            foreach($data as $key => $value) {
	                //If it is an associative array we want it in the format of "key":"value"
	                if(count($data) > 1) $json_str .= "\"$key\":\"$value\"";
	                else $json_str .= "\"$value\"";
	
	                //Make sure that the last item don't have a ',' (comma)
	                $count++;
	                if($count < count($data)) $json_str .= ",\n";
	            }
	            $row_count++;
	            if(count($data) > 1) $json_str .= "}\n";
	
	            //Make sure that the last item don't have a ',' (comma)
	            if($row_count < $total) $json_str .= ",\n";
	        }
	
	        $json_str .= "]\n";
	    }
	
	    //Replace the '\n's - make it faster - but at the price of bad redability.
	    $json_str = str_replace("\n","",$json_str); //Comment this out when you are debugging the script
	
	    //Finally, output the data
	    return $json_str;
	    }
	}	
	//
	////////////////////////

?>