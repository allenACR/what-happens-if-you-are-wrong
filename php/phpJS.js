

	////////////////////////
	// 	
	console.log("PHPJS loaded!");
	phpJS = {
		
		url: 'php/phpCore.php',


		/////////////////
		getIPAddress: function(callback){
	       $.ajax({ url: phpJS.url,
	                 data: {action: "getIPAddress"},
	                 type: 'post',
	                 success: function(data) {
						callback(true, data);	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 
		},
		/////////////////
		
		/////////////////
		getPHPStatus: function(callback){
	       $.ajax({ url: phpJS.url,
	                 data: {action: "getPHPStatus"},
	                 type: 'post',
	                 success: function(data) {
						callback(true, data);	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 
		},
		/////////////////
		
		// CREATE NEW PAGE
	    testFunction: function(callback) {   	
	       $.ajax({ url: phpJS.url,
	                 data: {action: "testFunction"},
	                 type: 'post',
	                 success: function(data) {
						callback(true, data);	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	        
	    },   
	    /////////////////
	    
		// CREATE NEW PAGE
	    createNewEntry: function(packet, values, callback) { 	 	 
		 	  
			
		    $.ajax({ url: phpJS.url,
		                 data: {action: "createNewEntry", database: packet.database, table: packet.table, values: values},
		                 type: 'post',
		                 success: function(data) {
							callback(true, JSON.parse(data) );	
		                 },
						 error: function(data){
							callback(false, JSON.parse(data) ) ;  
						 }
		    });	 
	    },  	    
	    
	    
	    
		// CREATE NEW PAGE
	    getTableFields: function(packet, callback) { 	 	 
	 
	       $.ajax({ url: phpJS.url,
	                 data: {action: "getTableFields", database: packet.database, table: packet.table},
	                 type: 'post',
	                 success: function(data) {
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data) ) ;  
					 }
	        });	        
	    },  
	    	 
		// CREATE NEW PAGE
	    queryDatabase2: function(packet, callback) { 

	       $.ajax({ url: phpJS.url,
	                 data: {action: "queryDatabase2", database: packet.database, query: packet.query},
	                 type: 'post',
	                 success: function(data) {
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data)) ;  
					 }
	        });	        
	    },   
	    /////////////////	 
	    	 
		// CREATE NEW PAGE
	    queryDatabase: function(packet, callback) { 

	       $.ajax({ url: phpJS.url,
	                 data: {action: "queryDatabase", database: packet.database, query: packet.query},
	                 type: 'post',
	                 success: function(data) {	          
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data)) ;  
					 }
	        });	        
	    },   
	    /////////////////	    
	    
		// CREATE NEW PAGE
	    sendEmail: function(emailData, callback) { 
	    	   
	       $.ajax({ url: phpJS.url,
	                 data: {action: "sendEmail", sendTo: emailData.sendTo, subject: emailData.subject, content: emailData.content, replyTo: emailData.replyTo},
	                 type: 'post',
	                 success: function(data) {
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data) ) ;  
					 }
	        });	        
	    },   
	    /////////////////	
	    
	    
		// CREATE NEW PAGE
	    basicUpload: function(file, nameOfFile, fileLocation, callback) {	
	    	
	    	if (fileLocation == null || fileLocation == undefined){
	    		fileLocation = "";
	    	};
	    	
			var reader = new FileReader();
	        reader.onload = function(readerEvt) {
	            var base64 = readerEvt.target.result;
		       $.ajax({ url: phpJS.url,
		                 data: {action: "basicUpload", base64: base64, nameOfFile: nameOfFile, fileLocation: fileLocation},
		                 type: 'post',
		                 success: function(data) {
							callback(true, JSON.parse(data));	
		                 },
						 error: function(data){
							callback(false, data) ;  
						 }
		        });	 
	        };
	        reader.readAsDataURL(file);

	    },   
	    /////////////////	        
   
	};
	//
	////////////////////////		
	

	
